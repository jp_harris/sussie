import unittest

from PySide6 import QtWidgets

# import logging
# logging.basicConfig(level=logging.DEBUG)

from oshydro.sussie.app.tabs.info.about.about_dialog import AboutDialog
from oshydro.sussie.app.tabs.info.about.tabs.general_info import GeneralInfoTab
from oshydro.sussie.app.tabs.info.about.tabs.license import LicenseTab
from oshydro.sussie.app.tabs.info.about.tabs.local_environment import LocalEnvironmentTab


class TestAppAboutDialog(unittest.TestCase):

    def test_visibility(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        d = AboutDialog()
        d.show()
        d.switch_visible()
        d.switch_visible()

        # noinspection PyArgumentList
        # QtWidgets.QApplication.instance().exec_()


class TestAppAboutDialogGeneralInfoTab(unittest.TestCase):

    def test_visibility(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        t = GeneralInfoTab()
        t.show()


class TestAppAboutDialogLicenseTab(unittest.TestCase):

    def test_visibility(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        t = LicenseTab()
        t.show()


class TestAppAboutDialogLocalEnvironmentTab(unittest.TestCase):

    def test_visibility(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        t = LocalEnvironmentTab()
        t.show()


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppAboutDialog))
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppAboutDialogGeneralInfoTab))
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppAboutDialogLicenseTab))
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppAboutDialogLocalEnvironmentTab))
    return s
