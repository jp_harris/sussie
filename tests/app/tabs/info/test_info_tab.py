import unittest
import platform

from PySide6 import QtWidgets

# import logging
# logging.basicConfig(level=logging.DEBUG)

from oshydro.sussie.app.tabs.info.info_tab import InfoTab


class TestAppTabsInfoTab(unittest.TestCase):

    def test_show(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        mw = QtWidgets.QMainWindow()

        t = InfoTab(main_win=mw,
                    with_online_manual=False,
                    with_offline_manual=False,
                    with_bug_report=True,
                    with_oshydro_link=True,
                    )
        t.show()

        # noinspection PyArgumentList
        # QtWidgets.QApplication.instance().exec_()


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppTabsInfoTab))
    return s
