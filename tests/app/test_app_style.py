import os
import unittest

from oshydro.sussie.app.app_style import AppStyle


class TestAppStyle(unittest.TestCase):

    def test_init(self):
        self.assertTrue(os.path.exists(AppStyle.here))
        self.assertTrue(os.path.exists(AppStyle.media))

    def test_html_css(self):
        self.assertGreater(len(AppStyle.html_css()), 10)

    def test_load_stylesheet(self):
        self.assertGreater(len(AppStyle.load_stylesheet()), 10)


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAppStyle))
    return s
