import unittest

from oshydro.sussie import __version__


class TestVersion(unittest.TestCase):

    def test_explore_folder(self):
        self.assertEqual(len(__version__.split('.')), 3)


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestVersion))
    return s