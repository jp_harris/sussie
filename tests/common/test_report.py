import os
import unittest

from PySide6 import QtWidgets

from oshydro.sussie.common.report import Report
from oshydro.sussie.common.testing import Testing


class TestReport(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.testing = Testing()

    def test_init(self):

        # noinspection PyArgumentList
        if not QtWidgets.QApplication.instance():
            import sys
            sys.argv.append("--disable-web-security")
            QtWidgets.QApplication(sys.argv)

        rep = Report()

        rep += "Test [CHECK]"
        rep += "OK"

        rep += "Test [CHECK]"
        rep += "test"
        rep += "test"
        rep += "test"

        rep += "test [SKIP_CHK]"

        rep += "skip [SKIP_REP]"

        rep += "End [TOTAL]"
        rep += "Check 1 - Test"
        rep += "Check 2 - Test"

        rep.display()

        rep.generate_pdf(path=os.path.join(self.testing.output_data_folder(), 'test.pdf'),
                         title="Test Document", use_colors=True, small=True)


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestReport))
    return s
