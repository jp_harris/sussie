import codecs
import os
import re

# Always prefer setuptools over distutils
from setuptools import setup, find_packages

# ------------------------------------------------------------------
#                         HELPER FUNCTIONS

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M, )
    if version_match:
        return version_match.group(1)

    raise RuntimeError("Unable to find version string.")


# ------------------------------------------------------------------
#                          POPULATE SETUP

setup(
    name="oshydro.sussie",
    version=find_version("oshydro", "sussie", "__init__.py"),
    license="MIT license",

    namespace_packages=["oshydro"],
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests", "*.test*", ]),
    package_data={
        "": ["media/*.png", "media/*.ico", "media/*.icns", "media/*.txt",],
    },
    zip_safe=False,
    setup_requires=[
        "setuptools",
        "wheel",
    ],
    install_requires=[
        "appdirs",
        "gdal",
        "matplotlib",
        "numpy",
        "pyshp",
        "PySide6",
    ],
    python_requires='>=3.7',
    entry_points={
        "gui_scripts": [
            'sussie = oshydro.sussie.app.gui:gui',
        ],
        "console_scripts": [
        ],
    },
    test_suite="tests",

    description="A tool to handle hydrographic survey data.",
    long_description=(read("README.rst")),
    url="https://github.com/hyo2/sussie",
    classifiers=[  # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering :: GIS',
        'Topic :: Office/Business :: Office Suites',
    ],
    keywords="hydrography ocean mapping survey data quality",
    author="Giuseppe Masetti(GST); Philip Sigaard Christiansen(GST); Ove Andersen(GST); James Peter Harris(GST); Lasse Mondberg Schwenger (GST)",
    author_email="gimas@gst.dk; phsic@gst.dk; ovand@gst.dk; japeh@gst.dk; lamon@gst.dk",
)
