Sussie
======

The Support Utility for Survey Submission Integrity Evaluation (Sussie) tool provides functionalities
to handle hydrographic survey data.

Sussie has 3 main components:

* Submission Checker: evaluate the folder/file structure of the submitted survey.
* Raw Data Explorer: explore the binary contet of raw data files.

The Sussie project is part of the Open Source Hydro (`OSHydro <https://www.oshydro.org>`_) initiative.

How to release Sussie
======

Any request that requires changes to `SUSSIE <https://geodatastyrelsen.atlassian.net/wiki/spaces/HYDEV/pages/1343455528>`_ can be addressed by creating a Jira ticket in `HYDEV Jira <https://geodatastyrelsen.atlassian.net/jira/software/c/projects/HYDEV/boards/505>`_ with the label sussie

[RELEVANT PERSON] = The one doing the change to the code

[LINK TO SUSSIE] = `Install Sussie <https://geodatastyrelsen.atlassian.net/wiki/spaces/HYDEV/pages/1343455528>`_

[DEVELOPER TEAM] = `Philip S. Christiansen <https://geodatastyrelsen.atlassian.net/wiki/people/557058:821c2852-948d-4478-9116-030bca269533?ref=confluence>`_, `Giuseppe Masetti <https://geodatastyrelsen.atlassian.net/wiki/people/5db698ce6acc0b0c2f112709?ref=confluence>`_, `Ove Andersen <https://geodatastyrelsen.atlassian.net/wiki/people/5f49021791e67a003f0b7928?ref=confluence>`_, `James Peter Harris <https://geodatastyrelsen.atlassian.net/wiki/people/61f3a0d278b7fd00720332ab>`_

[RELEASE MANAGER] = a member of [DEVELOPER TEAM]



Bitbucket repo: `Sussie <https://bitbucket.org/geodatastyrelsen/sussie/src/master/>`_



Step-by-step workflow:
------------------


- 1. Propose

    A need for a change that affects all users is noticed by team HYDRO. A Jira ticket is created in Jira and assigned to [RELEVANT PERSON] .

    These changes include (but are not limited to):

    Changes to source code

    Changes to configuration files

    Bug fixes


- 2. Implement

    Change is implemented to relevant files/code by [RELEVANT PERSON], tested and finally committed. The test is done in the most exhaustive way as possible, by e.g. utilizing all available test surveys. All necessary changes are made, including filling in the commit message (mandatory).

- 3. Review

    The [RELEVANT PERSON] creates a pull request and assign a reviewer(s) from [DEVELOPER TEAM] in BitBucket.

- 4. Test

    The reviewer evaluates the changes and runs the code with sufficient amount of test data or relevant tests and comments the pull request if needed. [RELEVANT PERSON] will get a notification if commented. The changes can be merged by [RELEVANT PERSON] when all comments are resolved, if any.

- 5. Document

    The [RELEVANT PERSON] updates the markdown files found in ``<path_to_Sussie>\Sussie\docs`` to reflect changes to the current code, updated version numbers and updating the year on the copyright.

    The documentation is then built to a ``.pdf`` file using MikTex.

    Using a shell with hde_3, in ``<path_to_Sussie>\Sussie\docs``, run ``make pdf``

    This requires MikTex to be installed [Download]. The first time building, it will need to download all the packages used. This can take some time.

    The resulting .pdf found in ``<path_to_Sussie>\Sussie\docs\_build\latex``  is then moved to ``\Sussie\oshydro\sussie\app\tabs\info\media\`` , so that it can be accessed from the offline manual button.

    The documentation is then built into a collection of .html files for online access.

    Using a shell with hde_3, in ``<path_to_Sussie>\Sussie\docs``, run ``make html``

    this produces ``.html`` files under ``\_build\html``, and a collection of files named ``_images``, ``_sources`` and ``_static``. It is important that when uploading these for online access, the underscores before each of these three names are removed - else the page will display in raw ``.html``. Underscores may be kept for local testing. This has documented in a bug report (with workaround) here.

    The ``.html`` files and folders are then added to the OSHydro `repository <https://github.com/OSHydro/OSHydro.github.io>`_, under ``\manuals\sussie``. (copy-paste the entire directory)

    ``index.html`` can be opened and used to check the functionality, structure and contents.

    commit and push these changes in the same was as the Sussie repository.

- 6. Release

    [DEVELOPER TEAM] decides whether all the required pending tickets have been implemented for making a new release. If yes, a [RELEASE MANAGER] is identified.

    [RELEASE MANAGER] has to perform the following step in preparation of a new release:

    Verify that all the required pending PRs have been merged.

    Switch to the current upstream ``'master'`` branch.

    Run the test suite from . Fix errors before moving to the next step.

    Command, from a shell with hde_3: ``python -m unittest discover -s <path_to_Sussie>\Sussie\tests -t <path_to_Sussie>\Sussie\tests``

    Decides on the versioning before the release of tools to users (both at the package level and per-tool). The version is chosen according to: [X. X. X] = [major.minor.patch]. A version of the tool needs to be changed in case that any of the code or its dependencies were changed.

    Add a tag (e.g., release.1.4.0) to the current master and push it upstream.

    Create a frozen executable

    Using a shell with hde_3 in ``<path_to_Sussie>\Sussie, run pyinstaller --clean -y freeze\Sussie.spec``

    zip the produced folder, then try and install locally and test functionality from the executable. Remember to test documentation as well.

    ::
        as part of the development process, there may be links to pages that are not currently live (such as the changelog for the current version). These will not appear until made live, and so a 404 error here should not be cause for concern on its own.

    Update the markdown files in the OSHydro repository under ``OSHydro\projects\sussie`` to reflect the new version under ``latest.txt``, and the changes under the respective ``.md`` file (``index.md`` and ``x.y.z.md``).

    Commit these changes, and make a pull request to rebase them into the master. This makes the changes live and publicly accessible, also to the online manual included within Sussie

    Upload the .zip file to bitbucket

    Command: ``curl -v -s -u <bitbucket-username> -X POST https://api.bitbucket.org/2.0/repositories/geodatastyrelsen/sussie/downloads/ -F files=@Sussie.ver.si.on.zip``. Also found at the top of ``Sussie.spec``.

    The Sussie application should now also be available to the public via

- 7. Inform

    [RELEVANT PERSON] sends an email to ``DL-CU2713GST-SOE-Hydro@sitad.dk`` to inform users that they should download and install:

    The latest version of the tool from [LINK TO SUSSIE].

    Email template:

    Subject:  Sussie - Release <VERSION>

    Body:

    Dear HYDRO,

    Sussie (<VERSION>) has been released!

    Release highlights:

    [PR/Tickets-related achievements]

    Please download and install the new release according to instructions in ``https://geodatastyrelsen.atlassian.net/wiki/spaces/HYDEV/pages/1343455528``

    Update the Confluence page to reflect the new version