import json
import logging
import math
import os
from datetime import datetime
from enum import Enum
from typing import Any, Callable, List, Optional

from shapefile import Writer

from oshydro.sussie import name, __version__
from oshydro.sussie.common.helper import Helper
from oshydro.sussie.common.progress.abstract_progress import AbstractProgress
from oshydro.sussie.common.progress.cli_progress import CliProgress
from oshydro.sussie.common.report import Report
from oshydro.sussie.survey.gridding.raw_gridder import RawGridder
from oshydro.sussie.survey.position_analyzer.position_analyzer import PositionAnalyzer
from oshydro.sussie.survey.raw.formats.formats import Formats
from oshydro.sussie.survey.raw.raw import Raw
from oshydro.sussie.survey.submission_checks.schema import Schema

logger = logging.getLogger(__name__)


class Project:
    class OutputFormat(Enum):
        XYZ = 0
        SHP = 1

    class IssueType(Enum):
        DUPLICATES = 0
        OUTLIERS = 1

    def __init__(self, projects_folder: Optional[str] = None, progress: AbstractProgress = CliProgress()):

        Helper.check_supported_python(supported_versions=['3.7', '3.8', '3.9', '3.10'])

        # output folder
        self._output_folder = None  # the project's output folder
        if (projects_folder is None) or (not os.path.exists(projects_folder)):
            projects_folder = self.default_output_folder()
            logger.debug("using default output folder: %s" % projects_folder)
        self.output_folder = projects_folder

        # progress bar
        if not isinstance(progress, AbstractProgress):
            raise RuntimeError("invalid progress object")
        self.progress = progress

        self._submissions = list()

        self._exhaustive = True
        self._valid = True
        self._schema = None
        self._root_name = None
        self._root_path = None
        self._total_error_counter = 0
        self._total_error_files = 0
        self._error_counter = 0
        self._r = None  # report
        self._report_path = None  # type: Optional[str]

        self._bin_diff_list = list()
        self._sem_diff_list = list()

        self.computation_time = 0.0

        self._path_to_modify: Optional[str] = None
        self._protected_paths = list()

    def clear_data(self) -> None:

        self._exhaustive = True
        self._valid = True
        self._schema = None
        self._root_name = None
        self._root_path = None
        self._total_error_counter = 0
        self._total_error_files = 0
        self._error_counter = 0
        self._r = None  # report
        self._report_path = None

        self._bin_diff_list = list()
        self._sem_diff_list = list()

        self.computation_time = 0.0

        self._path_to_modify = None
        self._protected_paths = list()

    @property
    def exhaustive(self):
        return self._exhaustive

    @exhaustive.setter
    def exhaustive(self, value: bool):
        self._exhaustive = value

    @property
    def valid(self):
        return self._valid

    @classmethod
    def schema_folder(cls) -> str:

        return os.path.join(os.path.dirname(__file__), "submission_checks", "json")

    @classmethod
    def open_schema_folder(cls) -> None:
        Helper.explore_folder(cls.schema_folder())

    @classmethod
    def schema_list(cls) -> list:

        return Helper.files(folder=cls.schema_folder(), ext=".json")

    @classmethod
    def schema_dict(cls) -> dict:
        sl = cls.schema_list()
        sd = dict()
        for s in sl:
            sd[s] = "%s [v.%s]" % (Schema(s).title, Schema(s).version)
        return sd

    @property
    def schema(self) -> Schema:

        return self._schema

    @schema.setter
    def schema(self, value):

        self._schema = Schema(value)

    @property
    def submission_list(self):

        return self._submissions

    def add_to_submission_list(self, path):

        if not os.path.exists(path):
            logger.warning("the passed path does not exist: %s" % path)
            return

        if not os.path.isdir(path):
            logger.warning("the passed path is not a folder path: %s" % path)
            return

        if path in self._submissions:
            logger.warning("the passed folder path is already present: %s" % path)
            return

        self._submissions.append(path)

    def remove_from_submission_list(self, path):

        if path not in self._submissions:
            logger.warning("the passed folder path is not present: %s" % path)
            return

        self._submissions.remove(path)

    def clear_submission_list(self):
        self._submissions = list()

    @property
    def output_folder(self) -> str:
        return self._output_folder

    @output_folder.setter
    def output_folder(self, output_folder: str):
        if not os.path.exists(output_folder):
            raise RuntimeError("the passed output folder does not exist: %s" % output_folder)
        self._output_folder = output_folder

    def open_output_folder(self) -> None:
        Helper.explore_folder(self.output_folder)

    @classmethod
    def default_output_folder(cls) -> str:

        output_folder = Helper.package_folder()
        if not os.path.exists(output_folder):  # create it if it does not exist
            os.makedirs(output_folder)

        return output_folder

    @property
    def total_nr_of_errors(self) -> int:
        return self._total_error_counter

    @property
    def nr_of_errors(self) -> int:
        return self._error_counter

    def check_submission(self) -> bool:
        start_time = datetime.now()
        logger.info("Check submission folder ...")

        if len(self.submission_list) < 1:
            raise RuntimeError("first load at least one submission path")

        if self.schema is None:
            raise RuntimeError("first set the submission schema")

        self._total_error_counter = 0
        self._total_error_files = 0
        total_roots = len(self.submission_list)
        logger.debug("number of roots: %s" % total_roots)
        quantum = 90.0 / (total_roots * 2)
        self.progress.start(title="Submission checks v.%s" % __version__,
                            text="Checking %d root folders" % total_roots,
                            init_value=10)

        for idx, self._root_path in enumerate(self.submission_list):

            self._error_counter = 0

            self._validate_path(root_path=self._root_path)
            self.progress.add(quantum=quantum, text="Validating [%d/%d]" % (idx + 1, total_roots))

            self._r.generate_pdf(path=self._report_path, title="Submission Report",
                                 use_colors=True, small=True)
            if os.path.exists(self._report_path):
                Helper.explore_folder(self._report_path)
            self.progress.add(quantum=quantum, text="Writing report [%d/%d]" % (idx + 1, total_roots))

            self._total_error_counter += self._error_counter
            if self._error_counter > 0:
                self._total_error_files += 1

        logger.info('identified %d issues in %d files' % (self._total_error_counter, self._total_error_files))
        self.computation_time = (datetime.now() - start_time).total_seconds() / 60.0
        logger.info("Check submission folder ... DONE! -> time: %.2f mins" % self.computation_time)

        return self._total_error_counter == 0

    def _validate_path(self, root_path: str):
        self._root_name = os.path.basename(root_path)
        self._report_path = os.path.join(self.output_folder, "%s_submission_checks.%s.pdf"
                                         % (self._root_name, datetime.utcnow().strftime('%Y%m%d_%H%M%S')))
        if os.path.exists(self._report_path):
            os.remove(self._report_path)
        self._r = Report(lib_name=name, lib_version=__version__)

        if "root_validation" in self.schema.structure:
            self._validate_root()

        if self.valid or self.exhaustive:
            if "subs" not in self.schema.structure:
                self.progress.end()
                raise RuntimeError("unable to locate subs key in schema")

            for sub in self.schema.structure["subs"]:
                self.check_folder(parent_path=root_path, sub=sub)

        self._r += 'SUMMARY [TOTAL]'
        self._r += 'Identified errors: %s' % self.nr_of_errors

    def _validate_root(self) -> bool:
        logger.info("root validation: start")
        self._r += "Root Validation [CHECK]"
        logger.debug("root name: %s" % self._root_name)
        valid = True
        if "min_length" in self.schema.structure["root_validation"]:
            if len(self._root_name) < self.schema.structure["root_validation"]["min_length"]:
                logger.warning("root name too short")
                self._r += "Root name (%s) too short: %s < %s" \
                           % (self._root_name, len(self._root_name),
                              self.schema.structure["root_validation"]["min_length"])
                valid = False
                self._error_counter += 1
        if "max_length" in self.schema.structure["root_validation"]:
            if len(self._root_name) > self.schema.structure["root_validation"]["max_length"]:
                logger.warning("root name too long")
                self._r += "Root name (%s) too long %s > %s" \
                           % (self._root_name, len(self._root_name),
                              self.schema.structure["root_validation"]["max_length"])
                valid = False
                self._error_counter += 1
        if valid:
            self._r += "OK"
        self._valid = valid

        logger.info("root validation: end")
        return True

    def check_folder(self, parent_path: str, sub: dict):
        logger.debug("path validation %s: start" % (sub["name"]))

        valid = True
        path = os.path.normpath(os.path.join(parent_path, sub["name"]))
        self._r += "Folder Validation \"%s\" [CHECK]" % os.path.relpath(path, self._root_path)

        if not os.path.exists(path):
            logger.warning("unable to locate folder: %s" % path)
            self._r += "Missing or misnamed folder: %s" % path
            valid = False
            self._error_counter += 1

        if valid:
            self._r += "OK"

        if valid or self.exhaustive:
            if "subs" in sub:
                for item in sub["subs"]:
                    self.check_folder(parent_path=path, sub=item)

        self._valid = valid
        logger.debug("path validation %s: end" % (sub["name"]))

    @classmethod
    def search(cls, folder: str, exts: List[str]) -> List[str]:
        return Helper.files(folder=folder, ext=','.join(exts))

    def is_binary_equal(self, first_path: str, second_path: str) -> bool:
        if not os.path.exists(first_path):
            raise RuntimeError('Unable to locate %s' % first_path)
        if not os.path.exists(second_path):
            raise RuntimeError('Unable to locate %s' % second_path)

        is_equal = True
        buffer_size = 1024 * 1024

        self.progress.start(title='Binary comparison', text='Comparing bytes ...')

        first_size = os.path.getsize(first_path)
        second_size = os.path.getsize(second_path)
        min_size = min(first_size, second_size)
        logger.debug('min size: %s' % min_size)
        quantum = 95.0 / ((min_size // buffer_size) + 2)
        logger.debug('quantum: %s' % quantum)

        self._bin_diff_list = list()
        counter = 0
        offset = 0
        with open(first_path, 'rb') as f1, open(second_path, 'rb') as f2:

            loop = True
            while loop:
                buf1 = f1.read(buffer_size)
                buf2 = f2.read(buffer_size)
                if len(buf1) == 0 or len(buf2) == 0:
                    loop = False

                for idx in range(len(list(zip(buf1, buf2)))):
                    if buf1[idx] != buf2[idx]:
                        is_equal = False
                        self._bin_diff_list.append((offset, hex(buf1[idx]), hex(buf2[idx])))
                        counter += 1

                    offset += 1

                if counter != 0:
                    self.progress.add(quantum, text='Inequalities: %d' % counter)
                else:
                    self.progress.add(quantum)

            self.progress.end()

        if first_size != second_size:
            is_equal = False
            if first_size > second_size:
                self._bin_diff_list.append((min_size, '\u2193', 'EOF'))
            else:
                self._bin_diff_list.append((min_size, 'EOF', '\u2193'))
            counter += 1

        logger.debug('nr of differences: %d' % len(self._bin_diff_list))
        return is_equal

    @property
    def bin_diff_list(self) -> list:
        return self._bin_diff_list

    def is_semantically_equal(self, first_path: str, second_path: str) -> bool:
        if not os.path.exists(first_path):
            raise RuntimeError('Unable to locate %s' % first_path)
        if not os.path.exists(second_path):
            raise RuntimeError('Unable to locate %s' % second_path)

        self.progress.start(title='Semantical comparison', text='Comparing variant ...')

        self._bin_diff_list = list()
        counter = 0

        # Open file
        r1 = Raw(path=first_path)
        r2 = Raw(path=second_path)

        r1.check_variant()
        r2.check_variant()
        if r1.v['endianess']['result'] != r2.v['endianess']['result']:
            self._sem_diff_list.append(('variant',
                                        'different endianess: %s vs. %s'
                                        % (r1.v['endianess']['result'], r2.v['endianess']['result'])))
            counter += 1
        if (r1.header is None) != (r2.header is None):
            self._sem_diff_list.append(('variant',
                                        'different header presence: %s vs. %s'
                                        % ((r1.header is None), (r2.header is None))))
            counter += 1

        self.progress.add(5, text='Comparing header ...')

        r1.read_header()
        r2.read_header()
        if (r1.header is not None) and (r2.header is not None):
            for k, v in r1.header.d['fields'].items():
                if v != r2.header.d['fields'][k]:
                    if isinstance(v, bytes):
                        if len(v) > 30:
                            self._sem_diff_list.append(('header',
                                                        'different %s: %s[..] vs. %s[..]'
                                                        % (k, v[:30], r2.header.d['fields'][k][:30])))
                        else:
                            self._sem_diff_list.append(('header',
                                                        'different %s: %s vs. %s'
                                                        % (k, v, r2.header.d['fields'][k])))
                    else:
                        self._sem_diff_list.append(('header',
                                                    'different %s: %s vs. %s'
                                                    % (k, v, r2.header.d['fields'][k])))
                    counter += 1

        self.progress.add(5, text='Comparing body ...')

        r1.read_body()
        r2.read_body()
        nr_r1 = r1.body.nr_of_datagrams
        nr_r2 = r2.body.nr_of_datagrams
        if nr_r1 != nr_r2:
            self._sem_diff_list.append(('body',
                                        'different nr of datagrams: %s vs. %s'
                                        % (nr_r1, nr_r2)))
            counter += 1
        min_nr = min(nr_r1, nr_r2)
        quantum = 89.0 / (min_nr + 1)

        for idx in range(min_nr):
            dg1 = r1.body.get_datagram(idx=idx)
            dg2 = r2.body.get_datagram(idx=idx)

            nk1 = len(dg1['fields'])
            nk2 = len(dg2['fields'])
            if nk1 != nk2:
                self._sem_diff_list.append(('body',
                                            'different nr of keys: %s vs. %s'
                                            % (nk1, nk2)))
                counter += 1

            for k, v in dg1['fields'].items():
                if v != dg2['fields'][k]:
                    self._sem_diff_list.append(('body',
                                                '@%d different value for %s: %s vs. %s'
                                                % (idx, k, v, dg2['fields'][k])))
                    counter += 1

            if counter != 0:
                self.progress.add(quantum, text='Inequalities: %d' % counter)
            else:
                self.progress.add(quantum)

        self.progress.end()

        logger.debug('nr of differences: %d' % len(self._sem_diff_list))
        return counter == 0

    @property
    def sem_diff_list(self) -> list:
        return self._sem_diff_list

    def check_positions(self, paths: List[str], root_path: Optional[str],
                        check_outliers: bool = False, check_duplicates: bool = False,
                        full_json: bool = False) -> PositionAnalyzer:
        pos_analyzer = PositionAnalyzer(output_folder=self.output_folder, progress=self.progress)
        self._report_path = pos_analyzer.report_path
        pos_analyzer.check_positions(paths=paths, root_path=root_path, full_json=full_json,
                                     check_outliers=check_outliers, check_duplicates=check_duplicates)
        self._total_error_counter = pos_analyzer.errors
        self._total_error_files = pos_analyzer.error_files
        return pos_analyzer

    def fix_positions(self, json_path: str, paths: Optional[List[str]], root_path: Optional[str] = None,
                      remove_outliers: bool = False, remove_duplicates: bool = False) -> PositionAnalyzer:
        pos_analyzer = PositionAnalyzer(output_folder=self.output_folder, progress=self.progress)
        pos_analyzer.fix_positions(json_path=json_path, paths=paths, root_path=root_path,
                                   remove_outliers=remove_outliers, remove_duplicates=remove_duplicates)
        return pos_analyzer

    @classmethod
    def quick_validity_check(cls, path: str) -> None:

        if not os.path.exists(path):
            raise RuntimeError('Unable to locate: %s' % path)

        raw = Raw(path=path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("nr of datagrams: %s" % nr_of_dg)
        if raw.header is None:
            nr_beams = 0
            nr_pings = 0
        else:
            nr_beams = raw.header.nr_of_beams
            nr_pings = raw.header.nr_of_pings

        if (nr_pings == 0) or (nr_beams == 0):
            dg = raw.body.get_datagram(nr_of_dg - 1)
        else:
            if nr_of_dg != nr_beams * nr_pings:
                raise RuntimeError('ping/beam mismatch: %s != %s' % (nr_of_dg, nr_beams * nr_pings))
            dg = raw.body.get_sounding(ping=(nr_pings - 1), beam=(nr_beams - 1))

        if raw.body.leftover_bytes > 0:
            raise RuntimeError('Issue with file structure -> left-over bytes: %s' % raw.body.leftover_bytes)

        logger.debug('last datagram: %s' % dg)

        dgf = dg['fields']
        # logger.debug("sounding @%d -> %s" % (i, dgf))

        if not PositionAnalyzer.is_valid_position(dgf=dgf):
            raise RuntimeError('invalid position: (%s, %s)' % (dgf['n'], dgf['e']))

    def thin_files(self, input_paths: List[str], output_path: str, grid_size: float, skip_flagged: bool) -> None:
        rg = RawGridder(input_paths=input_paths, progress=self.progress)
        rg.make_grid(grid_res=grid_size, output_path=output_path, skip_flagged=skip_flagged)

    def export_files(self, input_paths: List[str], output_format: OutputFormat, skip_flagged: bool,
                     save_in_input_folder: bool) -> None:
        logger.debug('exporting %d files into %s' % (len(input_paths), output_format))
        logger.debug('skipping flagged points: %s' % (skip_flagged,))
        logger.debug('saving in input folder: %s' % (save_in_input_folder,))

        total_dg = 0
        for idx, input_path in enumerate(input_paths):
            raw = Raw(path=input_path)
            raw.check_variant()
            raw.read_header()
            raw.read_body()
            total_dg += raw.body.nr_of_datagrams
        self.progress.start(title='Export', text='Exporting files', max_value=total_dg, has_abortion=True)

        for idx, input_path in enumerate(input_paths):

            logger.info('%d/%d: exporting from %s'
                        % (idx + 1, len(input_paths), input_path))

            self._export_file(input_path=input_path, output_format=output_format, skip_flagged=skip_flagged,
                              save_in_input_folder=save_in_input_folder)
            if self.progress.canceled:
                break
            if save_in_input_folder:
                Helper.explore_folder(os.path.join(os.path.dirname(input_path), 'sussie_output'))

        if self.progress.canceled:
            logger.info('Exporting ... CANCELLED!')
            self.progress.end()
            return
        if not save_in_input_folder:
            self.open_output_folder()
        self.progress.end()

    def _export_file(self, input_path: str, output_format: OutputFormat, skip_flagged: bool,
                     save_in_input_folder: bool):
        raw = Raw(path=input_path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("nr of datagrams: %s" % nr_of_dg)

        if skip_flagged:
            flagged_txt = ""
        else:
            flagged_txt = ".with_flagged"

        if save_in_input_folder:
            output_folder = os.path.join(os.path.dirname(input_path), 'sussie_output')
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)
        else:
            output_folder = self.output_folder

        dg_pct_add = nr_of_dg // 10000  # Update every 10000 makes gui fairly responsive
        dg_pct = max(dg_pct_add, 1)
        dg_pct_added = 0
        self.progress.add(0, "Exporting %s" % input_path)

        if output_format == Project.OutputFormat.XYZ:
            output_file = os.path.join(output_folder,
                                       os.path.splitext(os.path.basename(input_path))[0] + flagged_txt + '.xyz')

            with open(output_file, 'w') as fod:
                for i, dgf_f in enumerate(raw.body.iter_datagrams()):
                    dgf = dgf_f["fields"]
                    # logger.debug("sounding @%d -> %s" % (i, dgf))

                    if self.progress.canceled:
                        return
                    if (i + 1) % dg_pct == 0:
                        self.progress.add(dg_pct_add)
                        dg_pct_added += dg_pct_add

                    if skip_flagged:
                        dgf_quality = dgf['quality']
                        if dgf_quality >= 127:
                            continue

                    dgf_n = dgf['n'] / 100.0
                    dgf_e = dgf['e'] / 100.0
                    dgf_depth = dgf['depth'] / 100

                    fod.write("%.3f %.3f %.2f\n" % (dgf_n, dgf_e, dgf_depth))

        elif output_format == Project.OutputFormat.SHP:
            output_file = os.path.join(output_folder,
                                       os.path.splitext(os.path.basename(input_path))[0] + flagged_txt + '.shp')

            with Writer(output_file) as w:
                w.field('Depth', 'N')
                for i, dgf_f in enumerate(raw.body.iter_datagrams()):
                    dgf = dgf_f["fields"]
                    # logger.debug("sounding @%d -> %s" % (i, dgf))
                    if self.progress.canceled:
                        return
                    if (i + 1) % dg_pct == 0:
                        self.progress.add(dg_pct_add)
                        dg_pct_added += dg_pct_add

                    if skip_flagged:
                        dgf_quality = dgf['quality']
                        if dgf_quality >= 127:
                            continue

                    dgf_n = dgf['n'] / 100.0
                    dgf_e = dgf['e'] / 100.0
                    dgf_depth = dgf['depth'] / 100

                    w.pointz(dgf_e, dgf_n, dgf_depth)
                    w.record(dgf_depth)

        else:
            raise RuntimeError('Unsupported output format: %s' % output_format)
        self.progress.add(nr_of_dg - dg_pct_added)
        logger.debug('output file: %s' % output_file)

    def make_modified_clone(self, input_path: str, output_path: str, hdr_modifier: Optional[Callable] = None,
                            dg_modifier: Optional[Callable] = None, max_dg_idx: Optional[int] = None) -> bool:

        self.progress.start(title='Make clone', text='Modifying ...')

        if os.path.exists(output_path):
            os.remove(output_path)
            logger.info('Removed existing %s' % output_path)

        raw = Raw(path=input_path)
        raw.check_variant()
        raw.read_header()
        raw.read_body()
        nr_of_dg = raw.body.nr_of_datagrams
        logger.debug("nr of datagrams: %s" % nr_of_dg)

        if raw.header:
            if max_dg_idx is not None:
                if (raw.header.nr_of_beams != 0) or (raw.header.nr_of_pings != 0):
                    leftover_beams = max_dg_idx % raw.header.nr_of_beams
                    if leftover_beams != 0:
                        raise RuntimeError('Invalid number of leftover beams: %s (%s)'
                                           % (leftover_beams, raw.header.nr_of_beams))
                    if max_dg_idx < nr_of_dg:
                        new_nr_ping = int(max_dg_idx / raw.header.nr_of_beams)
                        if raw.header.d['fields']['nr_of_swaths'] != new_nr_ping:
                            logger.debug('updating nr of ping: %s' % new_nr_ping)
                            raw.header.d['fields']['nr_of_swaths'] = new_nr_ping
            if hdr_modifier:
                hdr_modifier(raw.header)
            raw.header.write(output_path=output_path)
        self.progress.add(10.0)

        if max_dg_idx is None:
            max_dg_idx = raw.body.nr_of_datagrams

        if dg_modifier:
            nr_of_quantum = 10
            dg_quantum = 90.0 / nr_of_quantum
            update_every = math.floor(max_dg_idx / nr_of_quantum)
            for idx in range(raw.body.nr_of_datagrams):
                if idx >= max_dg_idx:
                    break
                if idx % update_every == 0 and idx > 0:
                    self.progress.add(quantum=dg_quantum)
                dg = raw.body.get_datagram(idx)
                dg = dg_modifier(dg, idx)
                if dg is None:
                    continue
                raw.body.write_datagram(dg=dg, output_path=output_path, append=True, keep_open=True)

        else:
            # bulk copy all bytes
            buffer = raw.body.get_raw_datagrams(start_idx=0, end_idx=(max_dg_idx - 1))
            raw.body.write_raw_datagram(buffer=buffer, output_path=output_path, append=True, keep_open=True)
            self.progress.add(quantum=80)

        raw.body.close_source()
        raw.body.ends_writing()
        self.progress.end()

        return True

    def convert_from_xyz(self, input_path: str, output_path: str, skip_rows: int = 0,
                         little_endian: bool = True,
                         depth_shift: Optional[float] = None,
                         easting_idx: int = 0, northing_idx: int = 1, depth_idx: int = 2,
                         split_value: str = ',', z_is_elevation: bool = False
                         ) -> bool:

        logger.debug('Settings: skip rows %s, little endian %s, depth shift %s'
                     % (skip_rows, little_endian, depth_shift))
        logger.debug('Settings: easting %s, northing %s, depth %s'
                     % (easting_idx, northing_idx, depth_idx))
        logger.debug('Settings: split value "%s", z is elevation %s'
                     % (split_value, z_is_elevation))

        self.progress.start(title='Conversion from XYZ', text='Preparation ...')

        if os.path.exists(output_path):
            os.remove(output_path)
            logger.info('Removed existing %s' % output_path)
        self.progress.add(10.0)

        raw = Raw(path=output_path)
        if little_endian:
            raw.force_little_endianess()
        else:
            raw.force_big_endianess()
        self.progress.add(10.0)

        ret = raw.convert_from_xyz(input_path=input_path, output_path=output_path, skip_rows=skip_rows,
                                   depth_shift=depth_shift, easting_idx=easting_idx, northing_idx=northing_idx,
                                   depth_idx=depth_idx, split_value=split_value, z_is_elevation=z_is_elevation)
        self.progress.end()
        return ret

    def convert_from_shp(self, input_path: str, output_path: str, little_endian: bool = True,
                         z_is_elevation: bool = False
                         ) -> bool:
        logger.debug('Settings: little endian %s, z is elevation %s' % (little_endian, z_is_elevation))

        self.progress.start(title='Conversion from SHP', text='Preparation ...')

        if os.path.exists(output_path):
            os.remove(output_path)
            logger.info('Removed existing %s' % output_path)
        self.progress.add(10.0)

        raw = Raw(path=output_path)
        if little_endian:
            raw.force_little_endianess()
        else:
            raw.force_big_endianess()
        self.progress.add(10.0)

        ret = raw.convert_from_shp(input_path=input_path, output_path=output_path, z_is_elevation=z_is_elevation)
        self.progress.end()

        return ret

    def seek_for_bytes(self, input_paths: List[str], seek_value: Any, seek_type: str,
                       endianess: Formats.Endianess) -> None:

        self.progress.start(title='Seek bytes', text='Searching ...')

        nr_of_quantum = len(input_paths)
        quantum = 99.0 / nr_of_quantum
        for input_path in input_paths:
            raw = Raw(path=input_path)
            raw.seek_for_bytes(seek_value=seek_value, seek_type=seek_type,
                               endianess=endianess)
            self.progress.add(quantum)

        self.progress.end()

    @property
    def report_path(self) -> Optional[str]:
        return self._report_path

    def __repr__(self):
        msg = "<%s>\n" % self.__class__.__name__

        msg += "  <input folders: %s>\n" % len(self.submission_list)
        msg += "  <valid: %s>\n" % self.valid
        if self.schema is None:
            msg += "  <schema: False>\n"
        else:
            msg += "  <schema: True (%s, %s)>\n" % (self.schema.title, self.schema.version)
        msg += "  <exhaustive: %s>\n" % self.exhaustive
        msg += "  <output folder: %s>\n" % self.output_folder

        return msg

    @property
    def protected_paths(self) -> List[str]:
        return self._protected_paths

    @protected_paths.setter
    def protected_paths(self, path) -> None:
        logging.info(f"{path} is now protected from certain methods ... ")
        self._protected_paths += path

    @protected_paths.deleter
    def protected_paths(self):
        del self._protected_paths

    def _print_point_issue_overview(self, js: dict, issue_type: IssueType) -> None:
        root_path = js["root_path"]

        if issue_type not in self.IssueType:
            raise ValueError("issue_type not valid. Must be from <class IssueType(Enum)>")

        issue_label = issue_type.name.lower()
        list_of_fau_files = list(js["checked"].keys())  # List of all files

        # print the files that need to be manually copied
        logger.info(f'Files with {issue_label}: ')
        for idx, path in enumerate(list_of_fau_files):
            nr_flagged = js["checked"][path][f'nr_of_flagged_{issue_label}']
            nr_unflagged = js["checked"][path][f'nr_of_unflagged_{issue_label}']
            nr_total = nr_flagged + nr_unflagged
            if nr_total > 0:
                logger.info(f' - {idx:03}: {os.path.join(root_path, path)} '
                            f'-> {nr_total} points ... {nr_flagged} (flagged) and {nr_unflagged} (unflagged)')

    def print_duplicate_overview(self, json_path: str) -> None:
        with open(json_path) as fid:
            js = json.load(fid)

        self._print_point_issue_overview(js, issue_type=self.IssueType.DUPLICATES)

    def print_outlier_overview(self, json_path: str) -> None:
        with open(json_path) as fid:
            js = json.load(fid)

        self._print_point_issue_overview(js, issue_type=self.IssueType.OUTLIERS)

    def fix_duplicates(self, json_path: str) -> None:

        with open(json_path) as fid:
            js = json.load(fid)

        root_path = js['root_path']

        if any(path in root_path for path in self._protected_paths):
            raise Exception("JSON is pointing to a protected path! "
                            "Copy files to another location and create new JSON."
                            f"Alternatively, remove the protected path ..."
                            f"\n JSON root path: {root_path}"
                            f"\n Protected paths: {self.protected_paths}")

        list_of_fau_files = list(js["checked"].keys())  # List of all files
        fau_files_to_fix = [f for f in list_of_fau_files if js["checked"][f]["duplicate_points"]]

        if '.minimal.' in json_path:
            raise Exception('Fixing duplicate points only takes full json as input')

        else:
            pos_analyzer = self.fix_positions(json_path=json_path,
                                              paths=fau_files_to_fix,
                                              remove_duplicates=True,
                                              root_path=root_path)
            report = pos_analyzer.report
            self._print_fixing_of_points(report, js, fau_files_to_fix, issue_type=self.IssueType.DUPLICATES)

    def fix_outliers(self, json_path: str) -> None:

        with open(json_path) as fid:
            js = json.load(fid)

        list_of_fau_files = list(js["checked"].keys())  # List of all files
        fau_files_to_fix = [f for f in list_of_fau_files if js["checked"][f]["outlier_points"]]

        root_path = js['root_path']

        if any(path in root_path for path in self._protected_paths):
            raise Exception("JSON is pointing to a protected path! "
                            "Copy files to another location and create new JSON."
                            f"Alternatively, remove the protected path ..."
                            f"\n JSON root path: {root_path}"
                            f"\n Protected paths: {self.protected_paths}")

        if '.minimal.' in json_path:
            raise Exception('Fixing outlier points only takes full json as input')
        else:
            pos_analyzer = self.fix_positions(json_path=json_path,
                                              paths=fau_files_to_fix,
                                              remove_outliers=True,
                                              root_path=root_path)
            report = pos_analyzer.report
            self._print_fixing_of_points(report, js, fau_files_to_fix, issue_type=self.IssueType.OUTLIERS)

    def _print_fixing_of_points(self, report: dict, js: dict, list_of_fau_files: List[str],
                                issue_type: IssueType) -> None:

        if issue_type not in self.IssueType:
            raise ValueError("issue_type not valid. Must be from <class IssueType(Enum)>")

        issue_label = issue_type.name.lower()

        for path in list_of_fau_files:
            fixed_points = len(report["checked"][path].get(f"{issue_label[:-1]}_points", set()))
            nr_unflagged = js["checked"][path][f"nr_of_unflagged_{issue_label}"]
            nr_flagged = js["checked"][path][f"nr_of_flagged_{issue_label}"]
            if fixed_points > 100:
                logger.warning(f"Too many fixed points to display for {path}: "
                               f"{fixed_points} ({nr_flagged} flagged + {nr_unflagged} unflagged)")

                del (report["checked"][path][f"{issue_label[:-1]}_points"])

        logger.debug('results: %s' % report)
        self.open_output_folder()

    def export_duplicates_to_fau(self, json_path: str) -> None:
        from oshydro.sussie.survey.raw.raw_header import RawHeader
        if '.minimal.' in json_path:
            raise Exception('Duplicate screening only takes full json as input')

        else:
            with open(json_path) as fid:
                js = json.load(fid)

            list_of_fau_files = list(js["checked"].keys())  # List of all files
            root_path = js['root_path']

            if any(path in root_path for path in self._protected_paths):
                raise Exception("JSON is pointing to a protected path! "
                                "Copy files to another location and create new JSON."
                                f"Alternatively, remove the protected path ..."
                                f"\n JSON root path: {root_path}"
                                f"\n Protected paths: {self.protected_paths}")

            def _dg_modify(dg: dict, idx: int) -> dict:
                # A helper function passed on to the method used to create a modified fau in project.py
                if not str(idx) in js['checked'][self._path_to_modify]['duplicate_points']:
                    dg = None
                return dg

            def _hdr_modify(hdr: RawHeader) -> None:
                # A helper function passed on to the method used to create a modified fau in project.py
                tmp = hdr.d['fields']['sv_name']
                hdr.d['fields']['sv_name'] = f'{tmp}_screened'.encode('utf-8')
                hdr.d['fields']['nr_of_beams'] = 0
                hdr.d['fields']['nr_of_pings'] = 0
                hdr.d['fields']['kind'] = 8

            for file in list_of_fau_files:
                self._path_to_modify = file
                self.make_modified_clone(input_path=os.path.join(root_path, file),
                                         output_path=os.path.join(root_path, file.replace(".fau", "_screened.fau")),
                                         hdr_modifier=_hdr_modify,
                                         dg_modifier=_dg_modify)

    def duplicate_points(self, json_path: str, print_overview: bool = True, fix_points: bool = False,
                         export_duplicates_to_fau: bool = False, protected_paths: Optional[List[str]] = None):
        if protected_paths:
            self.protected_paths = protected_paths

        if print_overview:
            self.print_duplicate_overview(json_path=json_path)

        if export_duplicates_to_fau:
            self.export_duplicates_to_fau(json_path=json_path)

        if fix_points:
            self.fix_duplicates(json_path=json_path)

    def positional_outliers(self, json_path: str, print_overview: bool = True, fix_points: bool = False,
                            protected_paths: Optional[List[str]] = None):
        if protected_paths:
            self.protected_paths = protected_paths

        if print_overview:
            self.print_outlier_overview(json_path=json_path)

        if fix_points:
            self.fix_outliers(json_path)
