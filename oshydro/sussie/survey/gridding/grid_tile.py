import logging
import numpy as np

logger = logging.getLogger(__name__)


class GridTile:

    def __init__(self):
        self.x = np.full([1000, 1000], np.nan)
        self.y = np.full([1000, 1000], np.nan)
        self.z = np.full([1000, 1000], np.nan)
