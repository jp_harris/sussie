import logging
import os
import struct
from typing import Optional, Generator

from oshydro.sussie.survey.raw.formats.formats import Formats
from oshydro.sussie.survey.raw.raw_header import RawHeader

logger = logging.getLogger(__name__)


class RawBody:

    def __init__(self, path: str, variant: dict, header: Optional[RawHeader]):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True

        self._p = path
        self._v = variant
        self._h = header

        self._keys = None
        self._types = str()
        self._dg_size = None
        self._hdr_size = 0

        if self._h:
            self._hdr_size = self._h.d['size']

        self._w = None

        if self._write_only:
            self._calc_keys_and_types(fields=self._v['body']['fields'],
                                      endianess=self._v['endianess']['result'])
            return

        self._p_size = os.stat(self._p).st_size
        # logger.debug('raw file size: %d bytes' % self._p_size)

        self._r = open(self._p, mode='rb')
        # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

        self._dg_type = Formats.Datagram(self._v['body']['type'])
        if self._dg_type == Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            # logger.debug("datagram type: %s" % self._dg_type)

            self._calc_keys_and_types(fields=self._v['body']['fields'],
                                      endianess=self._v['endianess']['result'])

            self._dg_size = struct.calcsize(self._types)
            # logger.debug('datagram size: %s' % self._dg_size)

            # logger.debug('header size: %d bytes' % self._hdr_size)
            self._nr_of_datagrams = (self._p_size - self._hdr_size) // self._dg_size
            # logger.debug('nr_of_datagrams: %d' % self._nr_of_datagrams)

            self._leftover_bytes = (self._p_size - self._hdr_size) % self._dg_size
            if self._leftover_bytes > 0:
                logger.warning('leftover bytes: %s' % self._leftover_bytes)
            else:
                # logger.debug('leftover bytes: %s' % self._leftover_bytes)
                pass

        else:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

    @property
    def nr_of_datagrams(self) -> int:
        return self._nr_of_datagrams

    @property
    def v(self) -> dict:
        return self._v

    @property
    def leftover_bytes(self) -> int:
        return self._leftover_bytes

    def get_sounding(self, ping: int, beam: int) -> dict:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if self._h is None:
            raise RuntimeError('missing header')

        if 0 < ping >= self._h.nr_of_pings:
            raise RuntimeError('Passed ping index is out of valid range: 0 < %s >= %s' % (ping, self._h.nr_of_pings))
        if 0 < beam >= self._h.nr_of_beams:
            raise RuntimeError('Passed beam index is out of valid range: 0 < %s >= %s' % (beam, self._h.nr_of_beams))

        if self._dg_type == Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            logger.debug("datagram type: %s" % self._dg_type)
            idx = ping * self._h.nr_of_beams + beam
            logger.debug('retrieving datagram with index: %s' % idx)
            return self.get_datagram(idx)

        else:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

    def get_raw_datagram(self, idx: int) -> bytes:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if 0 <= idx >= self._nr_of_datagrams:
            raise RuntimeError('Datagram index is out of valid range: 0 <= %s >= %s' % (idx, self._nr_of_datagrams))

        if self._dg_type == Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            # logger.debug("datagram type: %s" % self._dg_type)
            # logger.debug('datagram size: %s' % self._dg_size)

            offset = self._hdr_size + idx * self._dg_size
            self._r.seek(offset, 0)

            return self._r.read(self._dg_size)

        else:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

    def get_raw_datagrams(self, start_idx: int, end_idx: int) -> bytes:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if 0 <= start_idx >= self._nr_of_datagrams:
            raise RuntimeError('Datagram start index is out of valid range: 0 <= %s >= %s'
                               % (start_idx, self._nr_of_datagrams))

        if 0 <= end_idx >= self._nr_of_datagrams:
            raise RuntimeError('Datagram end index is out of valid range: 0 <= %s >= %s'
                               % (end_idx, self._nr_of_datagrams))

        if end_idx < start_idx:
            raise RuntimeError('Datagram end index is smaller than start index: %s < %s'
                               % (end_idx, start_idx))

        if self._dg_type == Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            # logger.debug("datagram type: %s" % self._dg_type)
            # logger.debug('datagram size: %s' % self._dg_size)

            offset = self._hdr_size + start_idx * self._dg_size
            self._r.seek(offset, 0)

            return self._r.read(self._dg_size * (end_idx - start_idx + 1))

        else:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

    def get_datagram(self, idx: int) -> dict:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if 0 <= idx >= self._nr_of_datagrams:
            raise RuntimeError('Datagram index is out of valid range: 0 <= %s >= %s' % (idx, self._nr_of_datagrams))

        if self._dg_type == Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            # logger.debug("datagram type: %s" % self._dg_type)
            # logger.debug('datagram size: %s' % self._dg_size)

            offset = self._hdr_size + idx * self._dg_size
            self._r.seek(offset, 0)

            values = struct.unpack(self._types, self._r.read(self._dg_size))
            dg = dict()
            dg['fields'] = dict(zip(self._keys, values))
            return dg

        else:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

    def iter_datagrams(self) -> Generator[dict, None, None]:
        """Yields all datagrams as dictionaries.
            Each datagram is packed as an dict({"fields":dg}
        :return: Datagrams
        """
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if self._dg_type != Formats.Datagram.FIXED_SIZE_AFTER_TOP_HEADER:
            raise RuntimeError('Unknown datagram type: %s' % self._dg_type)

        self._r.seek(self._hdr_size, 0)  # Seek to first datagram

        for values in struct.iter_unpack(self._types, self._r.read()):
            fields = dict(zip(self._keys, values))
            od = dict()
            od['fields'] = fields
            yield od

    def _calc_keys_and_types(self, fields: dict, endianess: Formats.Endianess):
        if self._keys is None:

            self._keys = list(fields.keys())
            self._types = str()
            if endianess == Formats.Endianess.LITTLE:
                self._types += '<'
            elif endianess == Formats.Endianess.BIG:
                self._types += '>'
            else:
                raise RuntimeError('Undefined endianess: %s' % endianess)

            for k in self._keys:
                self._types += fields[k]['type']
            # logger.debug('types: %s' % self._types)

    def generate_datagram(self) -> dict:
        dg = dict()
        dg['fields'] = dict()
        keys = self._v['body']['fields'].keys()
        for k in keys:
            t = self._v['body']['fields'][k]['type']
            if t in ['f', 'd']:
                dg['fields'][k] = 0.0
            else:
                dg['fields'][k] = 0

        return dg

    def write_datagram(self, dg: dict, output_path: str, append: bool = True, keep_open: bool = False):
        if keep_open:
            if self._w is None:
                if append:
                    self._w = open(output_path, mode='ab')
                    # logger.debug('opened in append mode: %s' % os.path.basename(output_path))
                else:
                    self._w = open(output_path, mode='wb')
                    # logger.debug('opened in write mode: %s' % os.path.basename(output_path))
        else:
            if append:
                self._w = open(output_path, mode='ab')
                # logger.debug('opened in append mode: %s' % os.path.basename(output_path))
            else:
                self._w = open(output_path, mode='wb')
                # logger.debug('opened in write mode: %s' % os.path.basename(output_path))

        self._w.write(struct.pack(self._types, *dg['fields'].values()))
        if not keep_open:
            self._w.close()

    def write_raw_datagram(self, buffer: bytes, output_path: str, append: bool = True, keep_open: bool = False):
        if keep_open:
            if self._w is None:
                if append:
                    self._w = open(output_path, mode='ab')
                    # logger.debug('opened in append mode: %s' % os.path.basename(output_path))
                else:
                    self._w = open(output_path, mode='wb')
                    # logger.debug('opened in write mode: %s' % os.path.basename(output_path))
        else:
            if append:
                self._w = open(output_path, mode='ab')
                # logger.debug('opened in append mode: %s' % os.path.basename(output_path))
            else:
                self._w = open(output_path, mode='wb')
                # logger.debug('opened in append mode: %s' % os.path.basename(output_path))

        self._w.write(buffer)
        if not keep_open:
            self._w.close()

    def ends_writing(self) -> None:
        if self._w:
            self._w.close()
            self._w = None

    def close_source(self):
        self._r.close()
