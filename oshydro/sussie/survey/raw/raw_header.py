import logging
import os
import struct

from oshydro.sussie.survey.raw.formats.formats import Formats

logger = logging.getLogger(__name__)


class RawHeader:

    def __init__(self, path: str, variant: dict):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True

        self._p = path
        self._v = variant

        keys = self._v['header']['fields'].keys()
        types = str()
        if self._v['endianess']['result'] == Formats.Endianess.LITTLE:
            types += '<'
        else:
            types += '>'

        for k in keys:
            # print(k)
            types += self._v['header']['fields'][k]['type']
        # logger.debug('types: %s' % types)
        self.read_size = struct.calcsize(types)

        self._d = dict()

        if self._write_only:
            return

        self._r = open(self._p, mode='rb')
        # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

        values = struct.unpack(types, self._r.read(self.read_size))
        self._d['fields'] = dict(zip(keys, values))
        # logger.debug("header: %s" % (self._d, ))
        # for k, v in self._d['fields'].items():
            # if isinstance(v, bytes):
                # logger.debug(" - %s -> %s" % (k, v.decode()))
            # else:
            # logger.debug(" - %s -> %s" % (k, v))

        if "size_field" in self._v['header']:
            field_size = self._d['fields'][self._v['header']['size_field']]
            size_valid = self.read_size == field_size
            if size_valid:
                self._d['size'] = field_size
            else:
                logger.warning("size validity issue: %s [%s == %s]" % (size_valid, self.read_size, field_size))
                self._d['size'] = self.read_size

        self._r.close()

    @property
    def d(self) -> dict:
        return self._d

    @property
    def v(self) -> dict:
        return self._v

    @property
    def nr_of_pings(self) -> int:
        if "nr_of_pings" in self._v['header']:
            return self._d['fields'][self._v['header']['nr_of_pings']]
        else:
            raise RuntimeError("Missing 'nr_of_pings' key in header dictionary")

    @nr_of_pings.setter
    def nr_of_pings(self, value: int) -> None:
        if "nr_of_pings" in self._v['header']:
            self._d['fields'][self._v['header']['nr_of_pings']] = value
        else:
            raise RuntimeError("Missing 'nr_of_pings' key in header dictionary")

    @property
    def nr_of_beams(self) -> int:
        if "nr_of_beams" in self._v['header']:
            return self._d['fields'][self._v['header']['nr_of_beams']]
        else:
            raise RuntimeError("Missing 'nr_of_beams' key in header dictionary")

    @nr_of_beams.setter
    def nr_of_beams(self, value: int) -> None:
        if "nr_of_beams" in self._v['header']:
            self._d['fields'][self._v['header']['nr_of_beams']] = value
        else:
            raise RuntimeError("Missing 'nr_of_beams' key in header dictionary")

    def generate_fau_header(self):
        self._d['fields'] = dict()
        for k in self._v['header']['fields'].keys():
            if k == "identity":
                if self._v['endianess']['result'] == Formats.Endianess.LITTLE:
                    self._d['fields']["identity"] = b"fau__uaf"
                else:
                    self._d['fields']["identity"] = b"_uaffau_"
                continue

            if k == "minilab":
                self._d['fields']["minilab"] = b"#utm32nNwgs84"
                continue

            if k == "length":
                self._d['fields']["length"] = self.read_size
                continue

            t = self._v['header']['fields'][k]['type']
            if 's' in t:
                self._d['fields'][k] = b""
            elif t in ['f', 'd']:
                self._d['fields'][k] = 0.0
            else:
                self._d['fields'][k] = 0

        if "size_field" in self._v['header']:
            field_size = self._d['fields'][self._v['header']['size_field']]
            self._d['size'] = field_size
        else:
            raise RuntimeError('Invalid header size')

    def write(self, output_path: str):

        w = open(output_path, mode='wb')
        logger.debug('opened in write mode: %s' % os.path.basename(output_path))

        types = str()
        if self._v['endianess']['result'] == Formats.Endianess.LITTLE:
            types += '<'
        else:
            types += '>'

        keys = self._v['header']['fields'].keys()
        for k in keys:
            # print(k)
            types += self._v['header']['fields'][k]['type']
        logger.debug('types: %s' % types)

        w.write(struct.pack(types, *self.d['fields'].values()))
        w.close()
