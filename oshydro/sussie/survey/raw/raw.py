import logging
import math
import os
import random
import struct
from datetime import datetime
from typing import Optional, Any

from osgeo import ogr
from oshydro.sussie.survey.raw.formats.formats import Formats
from oshydro.sussie.survey.raw.raw_body import RawBody
from oshydro.sussie.survey.raw.raw_header import RawHeader

logger = logging.getLogger(__name__)


class Raw:

    def __init__(self, path: str, ext: Optional[str] = None):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True
        self._p = path
        self._ext = ext
        if self._ext is None:
            self._ext = os.path.splitext(self._p)[-1]
            if '.bk' in self._ext:
                self._ext = os.path.splitext(os.path.splitext(self._p)[-2])[-1]
        try:
            self._f = Formats.ext_dict()[self._ext]
        except Exception as e:
            raise RuntimeError('Unable to retrieve format: %s' % e)
        try:
            self._v = self._f['variants'][0]
        except Exception as e:
            raise RuntimeError('Unable to retrieve variant: %s' % e)
        # logger.debug('using format: %s, variant: %s' % (self._f['name'], self._v['version']))

        self._skip_header = False
        self._h = None
        self._b = None

    @property
    def ext(self) -> str:
        return self._ext

    @property
    def v(self) -> dict:
        return self._v

    def check_variant(self):
        if self._write_only:
            raise RuntimeError('Write-only Mode')
        self._check_endianess()

    def _check_endianess(self):
        if self._v['endianess']['type'] == 0:
            self._v['endianess']['result'] = Formats.Endianess.LITTLE

        elif self._v['endianess']['type'] == 1:
            self._v['endianess']['result'] = Formats.Endianess.BIG

        elif self._v['endianess']['type'] == 2:

            r = open(self._p, mode='rb')
            # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

            if 'first_types' in self._v['endianess']:

                keys = ['value', ]
                types = self._v['endianess']['first_types']
                values = struct.unpack(types, r.read(struct.calcsize(types)))
                d = dict(zip(keys, values))
                if d['value'] == self._v['endianess']['first_is_little'].encode():
                    self._v['endianess']['result'] = Formats.Endianess.LITTLE
                    # logger.debug('detected little endianess')
                elif d['value'] == self._v['endianess']['first_is_big'].encode():
                    self._v['endianess']['result'] = Formats.Endianess.BIG
                    # logger.debug('detected big endianess')
                else:
                    logger.warning('unable to read the header -> assuming little endian & no header')
                    self._v['endianess']['result'] = Formats.Endianess.LITTLE
                    self._skip_header = True

            elif 'fu2_checks' in self._v['endianess']:

                keys = ['value', ]
                types = self._v['endianess']['fu2_checks']
                values = struct.unpack(types, r.read(struct.calcsize(types)))
                d1 = dict(zip(keys, values))
                values = struct.unpack(types, r.read(struct.calcsize(types)))
                d2 = dict(zip(keys, values))
                values = struct.unpack(types, r.read(struct.calcsize(types)))
                d3 = dict(zip(keys, values))
                values = struct.unpack(types, r.read(struct.calcsize(types)))
                d4 = dict(zip(keys, values))
                # d1 is northing, thus it should be positive
                # d2 is easting, thus it should be positive (that's may be wrong in GL where UTM zones were abused)
                # d3 is depth, thus we assume positive but less than 11,000 m (Mariana Tranch is 10,994 m)
                # d4 is time, thus it should be more that 0 (Unix Time starts in 1970) and less than current time
                if (d1['value'] > 0) and (d2['value'] > 0) and \
                        (d3['value'] > 0) and (d3['value'] < 1100000) and \
                        (d4['value'] > 0) and (d4['value'] < datetime.utcnow().timestamp()):
                    self._v['endianess']['result'] = Formats.Endianess.LITTLE
                    # logger.debug('detected little endianess')
                else:
                    self._v['endianess']['result'] = Formats.Endianess.BIG
                    # logger.debug('detected big endianess')

            else:
                raise RuntimeError('Missing a method to detect endianess is format that supports both ones')

    def force_little_endianess(self):
        self._v['endianess']['result'] = Formats.Endianess.LITTLE

    def force_big_endianess(self):
        self._v['endianess']['result'] = Formats.Endianess.BIG

    @property
    def header(self) -> Optional[RawHeader]:
        return self._h

    def read_header(self) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if self._skip_header:
            return

        # no header
        if not self._v['header']:
            return

        self._h = RawHeader(path=self._p, variant=self._v)

    @property
    def body(self) -> Optional[RawBody]:
        return self._b

    def read_body(self) -> None:

        if self._write_only:
            raise RuntimeError('Write-only Mode')

        # no header
        if not self._v['body']:
            raise RuntimeError('Missing body section in the json format description')

        self._b = RawBody(path=self._p, variant=self._v, header=self._h)

    @classmethod
    def datetime_from_unix_time_and_centisec(cls, unix_time: int, centisec: int):
        return datetime.utcfromtimestamp(unix_time + centisec / 100.0)

    def seek_for_bytes(self, seek_value: Any, seek_type: str, endianess: Formats.Endianess) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        p_size = os.stat(self._p).st_size
        logger.debug('raw file size: %d bytes' % p_size)

        r = open(self._p, mode='rb')
        logger.debug('opened in read mode: %s' % os.path.basename(self._p))

        types = str()
        if endianess == Formats.Endianess.LITTLE:
            types += '<'
        elif endianess == Formats.Endianess.BIG:
            types += '>'
        else:
            raise RuntimeError('Undefined endianess: %s' % endianess)

        types += seek_type

        search_size = struct.calcsize(types)
        logger.debug('search size: %s' % search_size)

        nr_of_quantum = 10
        update_every = math.floor(p_size / nr_of_quantum)

        found_items = list()

        offset = 0
        while True:
            try:

                r.seek(offset, 0)
                value = struct.unpack(types, r.read(search_size))[0]
                if value == seek_value:
                    found_items.append(offset)
                    logger.info('detection at #%d offset -> %s' % (offset, value))

                if offset % update_every == 0:
                    logger.debug('reading %s bytes (%.1f%%)' % (offset, offset / p_size * 100.0))

                if offset + search_size >= p_size:
                    logger.info('reached the end of file')
                    break
                else:
                    offset += 1

            except Exception as e:
                logger.info('stopped reading -> %s' % e)
                break

        logger.info('identified matching items: %d\n%s' % (len(found_items), found_items))

    def count_rows(self, path: str) -> int:
        lines = 0
        with open(path, 'rb') as fid:
            buf_size = 1024 * 1024
            read_f = fid.raw.read

            buf = read_f(buf_size)
            while buf:
                lines += buf.count(b'\n')
                buf = read_f(buf_size)

        return lines

    def convert_from_xyz(self, input_path: str, output_path: str, skip_rows: int = 0,
                         depth_shift: Optional[float] = None,
                         easting_idx: int = 0, northing_idx: int = 1, depth_idx: int = 2,
                         split_value: str = ',', z_is_elevation: bool = False) -> bool:

        # no header
        if not self._v['body']:
            raise RuntimeError('Missing body section in the json format description')

        self._b = RawBody(path=self._p, variant=self._v, header=self._h)

        # generate a datagram template
        dgt = self._b.generate_datagram()
        # logger.debug('generated datagram: %s' % dg)

        nr_rows = self.count_rows(input_path)
        logger.debug('nr of rows to read: %d' % nr_rows)
        quantum = int(nr_rows / 20) + 1
        counter = 0
        with open(input_path) as fid:
            for row in fid:
                counter += 1
                if counter <= skip_rows:
                    continue
                tokens = row.strip().split(split_value)
                if len(tokens) < 3:
                    logger.info('skipping invalid row: %s' % row)
                    continue
                # logger.debug(tokens)
                dg = dgt.copy()
                dg['fields']['e'] = int(float(tokens[easting_idx]) * 100)
                dg['fields']['n'] = int(float(tokens[northing_idx]) * 100)
                depth = int(float(tokens[depth_idx]) * 100)
                if z_is_elevation:
                    depth = -depth
                if depth_shift is not None:
                    depth += int(depth_shift) * 100
                dg['fields']['depth'] = depth

                self._b.write_datagram(dg=dg, output_path=output_path, append=True, keep_open=True)

                if counter % quantum == 0:
                    logger.debug('writing %.1f pct' % (counter / nr_rows * 100.0))

        self._b.ends_writing()

        logger.debug('added %s points' % counter)

        return True

    def convert_from_shp(self, input_path: str, output_path: str, z_is_elevation: bool = False) -> bool:

        # no header
        if not self._v['body']:
            raise RuntimeError('Missing body section in the json format description')

        self._b = RawBody(path=self._p, variant=self._v, header=self._h)

        # generate a datagram template
        dgt = self._b.generate_datagram()
        # logger.debug('generated datagram: %s' % dg)

        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(input_path, 0)
        layer = dataSource.GetLayer()
        nr_fts = layer.GetFeatureCount()
        logger.debug('nr of features to read: %d' % nr_fts)
        quantum = int(nr_fts / 20) + 1
        counter = 0

        for idx, feature in enumerate(layer):
            geom = feature.GetGeometryRef()
            # logger.debug(tokens)
            dg = dgt.copy()
            dg['fields']['e'] = int(float(geom.GetX()) * 100)
            dg['fields']['n'] = int(float(geom.GetY()) * 100)
            depth = int(float(geom.GetZ()) * 100)
            if z_is_elevation:
                depth = -depth
            dg['fields']['depth'] = depth

            self._b.write_datagram(dg=dg, output_path=output_path, append=True, keep_open=True)
            counter += 1

            if idx % quantum == 0:
                logger.debug('writing %.1f pct' % (idx / nr_fts * 100.0))

        self._b.ends_writing()

        logger.debug('added %s points' % counter)

        return True

    def generate_fake_fau(self, nr_of_soundings: int = 1024,
                          start_northing: int = 767777288, start_easting: int = 39777965,
                          start_depth: int = 2000, swath_size: int = 512,
                          delta_northing: int = 100, delta_easting: int = 20,
                          add_randomness: bool = False, with_spikes=True) -> bool:

        self._h = RawHeader(path=self._p, variant=self._v)
        self._h.generate_fau_header()
        self._h.write(output_path=self._p)

        if not self._v['body']:
            raise RuntimeError('Missing body section in the json format description')

        self._b = RawBody(path=self._p, variant=self._v, header=self._h)

        # generate a datagram template
        dgt = self._b.generate_datagram()
        # logger.debug('generated datagram: %s' % dg)

        logger.debug('nr of soundings to generate: %d' % nr_of_soundings)
        quantum = int(nr_of_soundings / 20) + 1
        r1 = 0
        r2 = 0
        nr_spikes = 0
        half_swath = int(swath_size / 2)
        for idx in range(nr_of_soundings):
            i = idx // swath_size
            j = idx % swath_size
            if add_randomness:
                r1 = random.randint(0, 2)
                r2 = random.randint(0, 2)
            # logger.debug(tokens)
            dg = dgt.copy()
            dg['fields']['n'] = start_northing + i * delta_northing + r1
            dg['fields']['e'] = start_easting + j * delta_easting + r2
            if (idx % 1000 == 0) and (idx != 0) and (random.randint(0, 1) == 1) and with_spikes:  # add spikes
                dg['fields']['depth'] = start_depth - random.randint(start_depth // 4, start_depth // 2)
                nr_spikes += 1
                logger.debug("%d -> added spike (%d)" % (idx, dg['fields']['depth']))
            else:
                dg['fields']['depth'] = start_depth + i + j + r1
            dg['fields']['sec'] = 1634568357 + i
            dg['fields']['angle'] = int((j - half_swath) / half_swath * 6500)
            dg['fields']['heave'] = i % 128
            dg['fields']['pitch'] = i % 128
            dg['fields']['roll'] = i % 128
            dg['fields']['quality'] = i % 15 + r2
            dg['fields']['amplitude'] = i % 15 + r1
            dg['fields']['centisec'] = i % 100

            self._b.write_datagram(dg=dg, output_path=self._p, append=True, keep_open=True)

            if idx % quantum == 0:
                logger.debug('writing %.1f pct' % (idx / nr_of_soundings * 100.0))

        self._b.ends_writing()

        logger.debug('nr of spikes: %d' % nr_spikes)

        logger.debug('generated %s soundings!' % nr_of_soundings)

        return True
