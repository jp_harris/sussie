import json
import logging
import os
from enum import IntEnum
from typing import List, Dict

from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class Formats:
    class Type(IntEnum):
        FLAT_WITH_TOP_HEADER = 0

    class Endianess(IntEnum):
        LITTLE = 0
        BIG = 1
        BOTH = 2

    class Datagram(IntEnum):
        FIXED_SIZE_AFTER_TOP_HEADER = 0

    here = os.path.normpath(os.path.dirname(__file__))

    @classmethod
    def new(cls, name: str = 'New', exts: List[str] = None, fmt_type: Type = Type.FLAT_WITH_TOP_HEADER) -> dict:
        od = dict()
        od['name'] = name
        if exts is None:
            od['exts'] = list()
        else:
            od['exts'] = exts
        od['type'] = int(fmt_type)
        od['variants'] = [cls.new_variant()]
        return od

    @classmethod
    def new_variant(cls, version: str = '1.0') -> dict:
        od = dict()
        od['version'] = version
        od['endianess'] = dict()
        od['header'] = dict()
        od['body'] = dict()
        return od

    @classmethod
    def set_both_endianess(cls, var: dict, first_types: str, first_is_little) -> None:
        var['endianess']['type'] = Formats.Endianess.BOTH
        var['endianess']['first_types'] = first_types
        var['endianess']['first_is_little'] = first_is_little

    @classmethod
    def list(cls) -> List[str]:
        return Helper.files(folder=cls.here, ext='.json')

    @classmethod
    def ext_dict(cls) -> Dict[str, dict]:
        ed = dict()

        for f in cls.list():
            od = cls.load_from_json(json_path=f)
            for ext in od['exts']:
                ed[ext] = od

        return ed

    @classmethod
    def dump_to_json(cls, fmt: dict) -> str:
        json_path = os.path.join(cls.here, '%s.json' % fmt['name'])
        with open(json_path, 'w') as fod:
            json.dump(fmt, fod, indent=4)
        return json_path

    @classmethod
    def load_from_json(cls, json_path: str) -> dict:
        with open(json_path) as fid:
            # noinspection PyTypeChecker
            od = json.load(fid, object_pairs_hook=dict)
        return od
