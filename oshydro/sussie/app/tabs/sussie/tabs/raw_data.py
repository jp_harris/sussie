import logging
import os
import traceback
from typing import List

from PySide6 import QtCore, QtGui, QtWidgets

from oshydro.sussie import __version__
from oshydro.sussie.app.gui_settings import GuiSettings
from oshydro.sussie.app.widgets.dg_viewer import DatagramViewer
from oshydro.sussie.app.widgets.file_modifier import FileModifier
from oshydro.sussie.app.widgets.files_exporter import FilesExporter
from oshydro.sussie.app.widgets.files_thinner import FilesThinner
from oshydro.sussie.app.widgets.ping_viewer import PingViewer
from oshydro.sussie.app.widgets.positional_checker import PositionChecker
from oshydro.sussie.survey.project import Project
from oshydro.sussie.survey.raw.raw import Raw

logger = logging.getLogger(__name__)


class RawDataTab(QtWidgets.QMainWindow):
    here = os.path.abspath(os.path.dirname(__file__))

    def __init__(self, parent_win: QtWidgets.QMainWindow, prj: Project):
        QtWidgets.QMainWindow.__init__(self)
        # Enable dragging and dropping onto the GUI
        self.setAcceptDrops(True)

        # store a project reference
        self.prj = prj
        self.parent_win = parent_win
        self.media = self.parent_win.media

        # ui
        self.panel = QtWidgets.QFrame()
        self.setCentralWidget(self.panel)
        self.vbox = QtWidgets.QVBoxLayout()
        self.panel.setLayout(self.vbox)

        # -- settings
        self.settings = QtWidgets.QGroupBox("Settings")
        self.settings.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        self.vbox.addWidget(self.settings)
        self._ui_settings()

        # -- content
        self.raw_list = None
        self.content = QtWidgets.QGroupBox("Raw Data")
        self.content.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        self.vbox.addWidget(self.content)
        self._ui_content()

        self._skipped_files = 0

    def _ui_settings(self):
        params_hbox = QtWidgets.QHBoxLayout()
        self.settings.setLayout(params_hbox)

        params_hbox.addStretch()

        # left

        l_vbox = QtWidgets.QVBoxLayout()
        params_hbox.addLayout(l_vbox)

        # select extension
        hbox = QtWidgets.QHBoxLayout()
        l_vbox.addLayout(hbox)
        text_set_exts = QtWidgets.QLabel("File extensions:")
        hbox.addWidget(text_set_exts)
        self.ext_fau = QtWidgets.QCheckBox('.fau')
        self.ext_fau.setChecked(True)
        hbox.addWidget(self.ext_fau)
        self.ext_fu2 = QtWidgets.QCheckBox('.fu2')
        self.ext_fu2.setChecked(True)
        hbox.addWidget(self.ext_fu2)
        hbox.addStretch()

        params_hbox.addStretch()

        # right

        r_vbox = QtWidgets.QVBoxLayout()
        params_hbox.addLayout(r_vbox)

        hbox = QtWidgets.QHBoxLayout()
        r_vbox.addLayout(hbox)

        button = QtWidgets.QPushButton()
        hbox.addWidget(button)
        button.setFixedHeight(GuiSettings.single_line_height())
        button.setFixedWidth(GuiSettings.text_button_width() + 24)
        button.setText("Search")
        button.setToolTip('Search for raw data files')
        # noinspection PyUnresolvedReferences
        button.clicked.connect(self.click_search)

        button = QtWidgets.QPushButton()
        hbox.addWidget(button)
        button.setFixedHeight(GuiSettings.single_line_height())
        button.setFixedWidth(GuiSettings.single_line_height())
        icon_info = QtCore.QFileInfo(os.path.join(self.media, 'small_info.png'))
        button.setIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
        button.setToolTip('Open the manual page')
        button.setStyleSheet("QPushButton { background-color: rgba(255, 255, 255, 0); }\n"
                             "QPushButton:hover { background-color: rgba(230, 230, 230, 100); }\n")
        # noinspection PyUnresolvedReferences
        button.clicked.connect(self.click_open_info)

        params_hbox.addStretch()

    def _ui_content(self):
        vbox = QtWidgets.QVBoxLayout()
        self.content.setLayout(vbox)

        self.raw_list = QtWidgets.QTableWidget()
        self.raw_list.setSortingEnabled(True)
        self.raw_list.setFocus()
        self.raw_list.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.raw_list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.raw_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        # noinspection PyUnresolvedReferences
        self.raw_list.customContextMenuRequested.connect(self.make_context_menu)
        # noinspection PyUnresolvedReferences
        self.raw_list.itemDoubleClicked.connect(self.load_file)
        vbox.addWidget(self.raw_list)

    def click_open_info(self):
        logger.debug("open info")
        message_box = QtWidgets.QMessageBox(self)
        message_box.setText('Info about the tool')
        message_box.setInformativeText(
            "1. Select the extensions of the raw data to explore.\n"
            "2. Push the 'Search' button to actually execute the file search.\n"
            "3. Once executed, you can interact by right-clicking on the listed raw data files.\n"
        )
        message_box.exec_()

    def click_search(self):
        """abstract the calling mechanism"""
        # sanity checks
        if len(self.prj.submission_list) == 0:
            msg = "First add one or more submission folders!"
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.warning(self, "Warning", msg, QtWidgets.QMessageBox.Ok)
            return

        exts = list()
        if self.ext_fau.isChecked():
            exts.append('.fau')
        if self.ext_fu2.isChecked():
            exts.append('.fu2')
        if len(exts) == 0:
            msg = "First add one or more file extensions!"
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.warning(self, "Warning", msg, QtWidgets.QMessageBox.Ok)
            return
        logger.debug('extensions to search: %s' % (exts,))

        try:
            self.prj.progress.start(title='Searching', text='Search for %s' % (exts,))

            raws = self.prj.search(self.prj.submission_list[0], exts=exts)
            logger.debug('raw files: %s' % len(raws))

            self.prj.progress.add(10)

            self._update_table(raws=raws)

            self.prj.progress.end()

            msg = 'Located %d raw data files.' % len(raws)
            if self._skipped_files > 0:
                msg += '\n%d files were skipped. See the windows shell for more info.' % self._skipped_files
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.information(self, "Search v.%s" % __version__, msg, QtWidgets.QMessageBox.Ok)

        except Exception as e:
            traceback.print_exc()
            logger.error("%s" % e)
            self.prj.progress.end()
            return False

    def _update_table(self, raws: List[str]) -> None:
        # prepare the table
        self.raw_list.setSortingEnabled(False)
        self.raw_list.clear()
        col_labels = ['path', 'mini-label', 'nr of pings', 'nr of beams', 'source', 'kind', 'tide',
                      'min depth', 'max depth', 'position jumps', 'version']
        self.raw_list.setColumnCount(len(col_labels))
        self.raw_list.setHorizontalHeaderLabels(col_labels)

        # populate the table
        nr_rows = len(raws)
        self.raw_list.setRowCount(len(raws))
        if nr_rows == 0:
            self.raw_list.resizeColumnsToContents()
            self.prj.progress.add(80)
            return

        token = 80.0 / nr_rows
        self._skipped_files = 0
        for i, raw_path in enumerate(raws):

            try:
                if os.stat(raw_path).st_size < 1:
                    self._skipped_files += 1
                    logger.warning('Too small file size: %s' % raw_path)
                    continue

                raw = Raw(path=raw_path)
                raw.check_variant()
                raw.read_header()

                item = QtWidgets.QTableWidgetItem("%s" % raw_path)
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 0, item)

                if raw.header is None:
                    continue

                item = QtWidgets.QTableWidgetItem(
                    "%s" % raw.header.d['fields']['minilab'].decode(errors='replace').rstrip("\x00"))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 1, item)

                item = QtWidgets.QTableWidgetItem()
                item.setData(QtCore.Qt.DisplayRole, int("%s" % raw.header.nr_of_pings))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 2, item)

                item = QtWidgets.QTableWidgetItem()
                item.setData(QtCore.Qt.DisplayRole, int("%s" % raw.header.nr_of_beams))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 3, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='source', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 4, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='kind', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 5, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='tide', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 6, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='bb_min_h', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 7, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='bb_max_h', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 8, item)

                item = QtWidgets.QTableWidgetItem(self._dec(key='ping_nr_pos_jump', raw=raw))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 9, item)

                item = QtWidgets.QTableWidgetItem(
                    "%d.%d" % (raw.header.d['fields']['major'], raw.header.d['fields']['minor']))
                item.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.raw_list.setItem(i, 10, item)

            except Exception as e:
                traceback.print_exc()
                self._skipped_files += 1
                logger.warning('Issue with %s: %s' % (raw_path, e))

            self.prj.progress.add(token)

        self.raw_list.setSortingEnabled(True)
        self.raw_list.resizeColumnsToContents()

    @classmethod
    def _dec(cls, key: str, raw: Raw) -> str:
        label = "%d" % raw.header.d['fields'][key]
        if "dict" in raw.header.v['header']['fields'][key]:
            if label in raw.header.v['header']['fields'][key]['dict']:
                label += ' \u2192 %s' % raw.header.v['header']['fields'][key]['dict'][label]
        if "multiplier" in raw.header.v['header']['fields'][key]:
            label = "%.2f" % (raw.header.d['fields'][key] * raw.header.v['header']['fields'][key]['multiplier'])
        return label

    def make_context_menu(self, pos):
        """Make a context menu to deal with profile specific actions"""

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()
        if len(rows) == 0:
            logger.debug('Not profile selected')
            return

        menu = QtWidgets.QMenu(parent=self)

        # noinspection PyArgumentList
        header_info_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                            "Show header", self, toolTip="Show the file header",
                                            triggered=self.show_header)
        menu.addAction(header_info_act)

        # noinspection PyArgumentList
        header_modify_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                              "Modify header", self, toolTip="Modify specific fields "
                                                                             "in the file header",
                                              triggered=self.modify_header)
        menu.addAction(header_modify_act)

        # noinspection PyArgumentList
        dgs_info_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                         "Show data by datagrams", self,
                                         toolTip="Show the file data by datagram",
                                         triggered=self.show_dgs)
        menu.addAction(dgs_info_act)

        # noinspection PyArgumentList
        pings_info_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                           "Show data by pings/beams", self,
                                           toolTip="Show the file data by pings and beams",
                                           triggered=self.show_pings)
        menu.addAction(pings_info_act)

        # noinspection PyArgumentList
        check_quick_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                            "Quick validity check", self,
                                            toolTip="Run a quick check to verify file validity",
                                            triggered=self.check_quick)
        menu.addAction(check_quick_act)

        # noinspection PyArgumentList
        check_positions_outlier_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                                        "Check positions (duplicates, outliers)", self,
                                                        toolTip="Check files for positional outliers (e.g., equator points) or duplicate positions (xyz and quality)",
                                                        triggered=self.check_positions)
        menu.addAction(check_positions_outlier_act)

        def handle_menu_hovered(action):
            # noinspection PyArgumentList
            QtWidgets.QToolTip.showText(QtGui.QCursor.pos(), action.toolTip(), menu, menu.actionGeometry(action))

        # noinspection PyUnresolvedReferences
        menu.hovered.connect(handle_menu_hovered)

        # single selection
        if len(rows) == 1:
            # noinspection PyArgumentList
            bin_equal_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                              "Binary equal to ..", self,
                                              toolTip="Is binary equal to an user-selected file?",
                                              triggered=self.check_binary_equal)
            menu.addAction(bin_equal_act)

            # noinspection PyArgumentList
            sem_equal_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                              "Semantically equal to ..", self,
                                              toolTip="Is semantically equal to an user-selected file?",
                                              triggered=self.check_semantically_equal)
            menu.addAction(sem_equal_act)

        # noinspection PyArgumentList
        export_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                       "Points Export", self,
                                       toolTip="Export the points into another format",
                                       triggered=self.export_files)
        menu.addAction(export_act)

        # noinspection PyArgumentList
        grid_act = QtGui.QAction(QtGui.QIcon(os.path.join(self.media, 'file.png')),
                                     "Points Thinning", self,
                                     toolTip="Reduce the number of points by gridding at a given resolution",
                                     triggered=self.thin_files)
        menu.addAction(grid_act)

        menu.exec_(self.raw_list.mapToGlobal(pos))

    def show_header(self):
        logger.debug('showing file header')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        for row in rows:

            raw_path = self.raw_list.item(row.row(), 0).text()

            raw = Raw(path=raw_path)
            raw.check_variant()
            raw.read_header()
            if raw.header is None:
                # noinspection PyCallByClass
                QtWidgets.QMessageBox.information(self, "Header", "This file has NO header!")
                return

            rv = DatagramViewer(data_dict=raw.header.d, var_dict=raw.header.v['header'],
                                parent=self, title='Header - %s' % raw_path)
            rv.show()

    def modify_header(self):
        logger.debug('modifying file header')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        for row in rows:

            raw_path = self.raw_list.item(row.row(), 0).text()

            raw = Raw(path=raw_path)
            raw.check_variant()
            raw.read_header()
            if raw.header is None:
                # noinspection PyCallByClass
                QtWidgets.QMessageBox.information(self, "Header", "This file has NO header!")
                return

            fm = FileModifier(input_path=raw_path, parent=self, title='File Modifier')
            fm.show()

    def show_dgs(self):
        logger.debug('showing file by datagrams')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        for row in rows:

            raw_path = self.raw_list.item(row.row(), 0).text()

            raw = Raw(path=raw_path)
            raw.check_variant()
            raw.read_header()
            raw.read_body()

            if raw.body.nr_of_datagrams == 0:
                msg = 'There are not data to display.'
                # noinspection PyCallByClass,PyArgumentList
                QtWidgets.QMessageBox.information(self, "Datagrams View v.%s" % __version__, msg,
                                                  QtWidgets.QMessageBox.Ok)

                return

            rv = PingViewer(header=raw.header, body=raw.body, parent=self, force_show_by_datagrams=True,
                            title='Datagrams - %s' % raw_path)
            rv.show()

    def show_pings(self):
        logger.debug('showing file by pings/beams')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        for row in rows:

            raw_path = self.raw_list.item(row.row(), 0).text()

            raw = Raw(path=raw_path)
            raw.check_variant()
            raw.read_header()
            raw.read_body()

            if raw.body.nr_of_datagrams == 0:
                msg = 'There are not data to display.'
                # noinspection PyCallByClass,PyArgumentList
                QtWidgets.QMessageBox.information(self, "Pings/Beams View v.%s" % __version__, msg,
                                                  QtWidgets.QMessageBox.Ok)

                return

            rv = PingViewer(header=raw.header, body=raw.body, parent=self, title='Pings/Beams - %s' % raw_path)
            rv.show()

    def check_quick(self):
        logger.debug('quick checking files')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        paths = list()
        for row in rows:
            raw_path = self.raw_list.item(row.row(), 0).text()
            paths.append(raw_path)

        if len(paths) == 0:
            logger.info('At least 1 file needs to be selected')
            return

        nr_files = len(paths)
        quantum = 99 / nr_files
        self.prj.progress.start(title='Quick check')
        for idx, input_fau in enumerate(paths):
            try:
                self.prj.quick_validity_check(input_fau)
            except Exception as e:
                traceback.print_exc()
                QtWidgets.QMessageBox.warning(self, "Quick check v.%s" % __version__,
                                              'Issue with %d/%d (%s): %s' % (idx, nr_files - 1, input_fau, e),
                                              QtWidgets.QMessageBox.Ok)
                self.prj.progress.end()
                return
            self.prj.progress.add(quantum=quantum)
            logger.debug('- %d/%d (%s)' % (idx, nr_files - 1, input_fau))

        self.prj.progress.end()
        msg = 'All files seem valid.'
        # noinspection PyCallByClass,PyArgumentList
        QtWidgets.QMessageBox.information(self, "Quick check v.%s" % __version__, msg, QtWidgets.QMessageBox.Ok)

    def check_positions(self):
        logger.debug('export files')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        paths = list()
        for row in rows:
            raw_path = self.raw_list.item(row.row(), 0).text()
            paths.append(raw_path)

        if len(paths) == 0:
            logger.info('At least 1 file needs to be selected')
            return

        pc = PositionChecker(files=paths, prj=self.prj, parent=self)
        pc.show()
        self.prj.clear_data()

    def check_binary_equal(self):
        logger.debug('checking binary equality')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()
        if len(rows) != 1:
            raise RuntimeError('A single file needs to be selected')

        raw_path = self.raw_list.item(rows[0].row(), 0).text()

        # noinspection PyCallByClass
        selection, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Select file to binary compare",
                                                             os.path.dirname(raw_path), 'Files (*.*)')
        if not selection:
            return

        ret = self.prj.is_binary_equal(first_path=raw_path, second_path=selection)
        logger.debug('binary equal: %s' % ret)
        # logger.debug('differences: %s' % (self.prj.bin_diff_list,))

        msg = 'Compared files:\n#1 %s\n#2 %s\n\nAre binary equal: %s\n' % (raw_path, selection, ret)
        if ret is False:
            nr_diff = len(self.prj.bin_diff_list)
            msg += 'Differences: %d\n\n' % (nr_diff,)
            msg += ' [Offset \u2192 File#1, File#2]\n'
            for i in range(nr_diff):

                tup = self.prj.bin_diff_list[i]
                msg += ' - %d \u2192 %s, %s\n' % (tup[0], tup[1], tup[2])

                if i == 10:
                    msg += ' ... \n'
                    break

        # noinspection PyCallByClass,PyArgumentList
        QtWidgets.QMessageBox.information(self, "Binary equal v.%s" % __version__, msg, QtWidgets.QMessageBox.Ok)

    def check_semantically_equal(self):
        logger.debug('checking semantical equality')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()
        if len(rows) != 1:
            raise RuntimeError('A single file needs to be selected')

        raw_path = self.raw_list.item(rows[0].row(), 0).text()

        # noinspection PyCallByClass
        selection, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Select file to semantically compare",
                                                             os.path.dirname(raw_path), 'Files (*.*)')
        if not selection:
            return

        ret = self.prj.is_semantically_equal(first_path=raw_path, second_path=selection)
        logger.debug('semantically equal: %s' % ret)
        # logger.debug('differences: %s' % (self.prj.bin_diff_list,))

        msg = 'Compared files:\n#1 %s\n#2 %s\n\nAre semantically equal: %s\n' % (raw_path, selection, ret)
        if ret is False:
            nr_diff = len(self.prj.sem_diff_list)
            msg += 'Differences: %d\n\n' % (nr_diff,)
            msg += ' [Offset \u2192 File#1, File#2]\n'
            for i in range(nr_diff):

                tup = self.prj.sem_diff_list[i]
                msg += ' - %s \u2192 %s\n' % (tup[0], tup[1])

                if i == 10:
                    msg += ' ... \n'
                    break

        # noinspection PyCallByClass,PyArgumentList
        QtWidgets.QMessageBox.information(self, "Semantically equal v.%s" % __version__, msg, QtWidgets.QMessageBox.Ok)

    def thin_files(self):
        logger.debug('thinning files')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        paths = list()
        for row in rows:
            raw_path = self.raw_list.item(row.row(), 0).text()
            paths.append(raw_path)

        if len(paths) == 0:
            logger.info('At least 1 file needs to be selected')
            return

        fe = FilesThinner(files=paths, prj=self.prj, parent=self)
        fe.show()

    def export_files(self):
        logger.debug('export files')

        # check if any selection
        rows = self.raw_list.selectionModel().selectedRows()

        paths = list()
        for row in rows:
            raw_path = self.raw_list.item(row.row(), 0).text()
            paths.append(raw_path)

        if len(paths) == 0:
            logger.info('At least 1 file needs to be selected')
            return

        fe = FilesExporter(files=paths, prj=self.prj, parent=self)
        fe.show()

    def load_file(self):
        logger.debug('currently unused')

    def folder_changed(self):
        logger.debug('folder changed')
        self.raw_list.clear()
