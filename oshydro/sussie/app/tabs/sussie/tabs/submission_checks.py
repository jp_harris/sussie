import logging
import os

from PySide6 import QtCore, QtGui, QtWidgets

from oshydro.sussie import __version__
from oshydro.sussie.app.gui_settings import GuiSettings
from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class SubmissionTab(QtWidgets.QMainWindow):
    here = os.path.abspath(os.path.dirname(__file__))

    def __init__(self, parent_win, prj):
        QtWidgets.QMainWindow.__init__(self)
        # Enable dragging and dropping onto the GUI
        self.setAcceptDrops(True)

        # store a project reference
        self.prj = prj
        self.parent_win = parent_win
        self.media = self.parent_win.media

        # ui
        self.panel = QtWidgets.QFrame()
        self.setCentralWidget(self.panel)
        self.vbox = QtWidgets.QVBoxLayout()
        self.panel.setLayout(self.vbox)

        # - Submission checks
        self.recursive_behavior = None
        self.submissionChecks = QtWidgets.QGroupBox("Submission Checks")
        self.submissionChecks.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        self.vbox.addWidget(self.submissionChecks)
        sc_hbox = QtWidgets.QHBoxLayout()
        self.submissionChecks.setLayout(sc_hbox)
        # -- parameters
        self.setParametersSC = QtWidgets.QGroupBox("Parameters")
        self.setParametersSC.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        sc_hbox.addWidget(self.setParametersSC)
        self._ui_parameters_sc()
        # -- execution
        self.executeSC = QtWidgets.QGroupBox("Execution")
        self.executeSC.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        sc_hbox.addWidget(self.executeSC)
        self._ui_execute_sc()

        self.vbox.addStretch()

    def click_open_schema(self):
        """ Open schema """
        schema_inv = {v: k for k, v in self.prj.schema_dict().items()}
        schema_path = schema_inv[self.json_schema.currentText()]
        logger.debug('open schema: %s' % schema_path)
        Helper.explore_folder(schema_path)

    def click_open_schema_folder(self):
        """ Open schema folder """
        logger.debug('open schema folder')
        self.prj.open_schema_folder()

    def _ui_parameters_sc(self):
        params_vbox = QtWidgets.QVBoxLayout()
        self.setParametersSC.setLayout(params_vbox)
        params_vbox.addStretch()

        # select schema
        hbox = QtWidgets.QHBoxLayout()
        params_vbox.addLayout(hbox)
        hbox.addStretch()
        text_set_schema = QtWidgets.QLabel("Schema:")
        hbox.addWidget(text_set_schema)
        text_set_schema.setMinimumWidth(48)
        self.json_schema = QtWidgets.QComboBox()
        self.json_schema.addItems(list(reversed(list(self.prj.schema_dict().values()))))
        hbox.addWidget(self.json_schema)
        # schema
        button_open_schema = QtWidgets.QPushButton()
        hbox.addWidget(button_open_schema)
        button_open_schema.setFixedHeight(GuiSettings.single_line_height())
        button_open_schema.setFixedWidth(GuiSettings.single_line_height())
        icon_info = QtCore.QFileInfo(os.path.join(self.media, 'file.png'))
        button_open_schema.setIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
        button_open_schema.setToolTip('Open the schema')
        # noinspection PyUnresolvedReferences
        button_open_schema.clicked.connect(self.click_open_schema)
        # schema folder
        button_open_schema_folder = QtWidgets.QPushButton()
        hbox.addWidget(button_open_schema_folder)
        button_open_schema_folder.setFixedHeight(GuiSettings.single_line_height())
        button_open_schema_folder.setFixedWidth(GuiSettings.single_line_height())
        icon_info = QtCore.QFileInfo(os.path.join(self.media, 'dir.png'))
        button_open_schema_folder.setIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
        button_open_schema_folder.setToolTip('Open the folder with the schemas')
        # noinspection PyUnresolvedReferences
        button_open_schema_folder.clicked.connect(self.click_open_schema_folder)
        hbox.addStretch()

        # knobs

        hbox = QtWidgets.QHBoxLayout()
        params_vbox.addLayout(hbox)
        hbox.addStretch()

        vbox = QtWidgets.QVBoxLayout()
        hbox.addLayout(vbox)
        vbox.addStretch()

        # knob row

        toggle_hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(toggle_hbox)
        # stretch
        toggle_hbox.addStretch()
        # behaviors
        self.toggle_behaviors = QtWidgets.QDial()
        self.toggle_behaviors.setNotchesVisible(True)
        self.toggle_behaviors.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.toggle_behaviors.setRange(0, 1)
        self.toggle_behaviors.setValue(1)
        self.toggle_behaviors.setFixedSize(QtCore.QSize(48, 48))
        self.toggle_behaviors.setInvertedAppearance(False)
        toggle_hbox.addWidget(self.toggle_behaviors)
        # stretch
        toggle_hbox.addStretch()

        hbox.addStretch()

        # label row

        label_hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(label_hbox)
        # stretch
        label_hbox.addStretch()
        # behaviors
        label_hbox.addSpacing(1)
        text_0 = QtWidgets.QLabel("Recursive")
        text_0.setAlignment(QtCore.Qt.AlignLeft)
        text_0.setFixedWidth(54)
        label_hbox.addWidget(text_0)
        text_1 = QtWidgets.QLabel("Exhaustive")
        text_1.setAlignment(QtCore.Qt.AlignRight)
        text_1.setFixedWidth(54)
        label_hbox.addWidget(text_1)
        label_hbox.addSpacing(1)
        # stretch
        label_hbox.addStretch()

        params_vbox.addStretch()

    def _ui_execute_sc(self):
        vbox = QtWidgets.QVBoxLayout()
        self.executeSC.setLayout(vbox)

        hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(hbox)

        hbox.addStretch()

        button = QtWidgets.QPushButton()
        hbox.addWidget(button)
        button.setFixedHeight(GuiSettings.single_line_height())
        button.setFixedWidth(GuiSettings.text_button_width() + 24)
        button.setText("Submission checks")
        button.setToolTip('Check the submission directory')
        # noinspection PyUnresolvedReferences
        button.clicked.connect(self.click_submission_checks)

        button = QtWidgets.QPushButton()
        hbox.addWidget(button)
        button.setFixedHeight(GuiSettings.single_line_height())
        button.setFixedWidth(GuiSettings.single_line_height())
        icon_info = QtCore.QFileInfo(os.path.join(self.media, 'small_info.png'))
        button.setIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
        button.setToolTip('Open the manual page')
        button.setStyleSheet("QPushButton { background-color: rgba(255, 255, 255, 0); }\n"
                             "QPushButton:hover { background-color: rgba(230, 230, 230, 100); }\n")
        # noinspection PyUnresolvedReferences
        button.clicked.connect(self.click_open_info)

        hbox.addStretch()

    def click_open_info(self):
        logger.debug("open info")
        message_box = QtWidgets.QMessageBox(self)
        message_box.setText('Info about the tool')
        message_box.setInformativeText(
            "1. Select the schema to use for the verification.\n"
            "2. Decide whether to adopt an exhaustive approach or to stop at the first identified error (recursive).\n"
            "3. Push the 'Submission checks' button to actually execute the verification.\n"
            "4. Once executed, the tool provides the number of errors and a PDF report.\n"
        )
        message_box.exec_()

    def click_submission_checks(self):
        """abstract the calling mechanism"""
        # sanity checks
        if len(self.prj.submission_list) == 0:
            msg = "First add one or more submission folders!"
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.warning(self, "Warning", msg, QtWidgets.QMessageBox.Ok)
            return

        exhaustive = self.toggle_behaviors.value() == 1
        logger.debug("exhaustive: %s" % exhaustive)
        schema_inv = {v: k for k, v in self.prj.schema_dict().items()}
        schema_path = schema_inv[self.json_schema.currentText()]
        logger.debug("schema: %s" % schema_path)

        try:

            self.prj.exhaustive = exhaustive
            self.prj.schema = schema_path

            ret = self.prj.check_submission()
            logger.debug("successfully validated: %s" % ret)

            if ret:
                msg = "Successfully validated!"
            else:
                msg = "Validation issues!\n\nNumber of errors: %d" % self.prj.total_nr_of_errors

            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.information(self,
                                              "Submission checks v.%s" % __version__,
                                              msg, QtWidgets.QMessageBox.Ok)

        except Exception as e:
            logger.error("%s" % e)
            self.prj.progress.end()
            return False

    def folder_changed(self):
        logger.debug('folder changed')
