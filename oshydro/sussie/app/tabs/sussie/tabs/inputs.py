from PySide6 import QtWidgets, QtGui, QtCore

import os
import logging

from oshydro.sussie.app.gui_settings import GuiSettings
from oshydro.sussie.app.widgets.xyz2fau import Xyz2Fau

logger = logging.getLogger(__name__)


class InputsTab(QtWidgets.QMainWindow):
    here = os.path.abspath(os.path.dirname(__file__))

    def __init__(self, parent_win, prj):
        QtWidgets.QMainWindow.__init__(self)

        # store a project reference
        self.prj = prj
        self.parent_win = parent_win

        # ui
        self.panel = QtWidgets.QFrame()
        self.setCentralWidget(self.panel)
        self.vbox = QtWidgets.QVBoxLayout()
        self.panel.setLayout(self.vbox)

        self.loadData = QtWidgets.QGroupBox("Survey Data folder  [drap-and-drop to add, right click to drop files]")
        # self.loadData.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        self.vbox.addWidget(self.loadData)

        vbox = QtWidgets.QVBoxLayout()
        self.loadData.setLayout(vbox)

        # add grids
        hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(hbox)
        text_input_folder = QtWidgets.QLabel("Survey Folder:")
        hbox.addWidget(text_input_folder)
        # text_add_grids.setFixedHeight(GuiSettings.single_line_height())
        text_input_folder.setMinimumWidth(64)
        self.input_folder = QtWidgets.QListWidget()
        hbox.addWidget(self.input_folder)
        self.input_folder.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.input_folder.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        # noinspection PyUnresolvedReferences
        self.input_folder.customContextMenuRequested.connect(self.make_grids_context_menu)
        self.input_folder.setAlternatingRowColors(True)
        # Enable dropping onto the input grid list
        self.input_folder.setAcceptDrops(True)
        self.input_folder.installEventFilter(self)
        button_input_folder = QtWidgets.QPushButton()
        hbox.addWidget(button_input_folder)
        button_input_folder.setFixedHeight(GuiSettings.single_line_height())
        button_input_folder.setFixedWidth(GuiSettings.single_line_height())
        button_input_folder.setText(" + ")
        button_input_folder.setToolTip('Add (or drag-and-drop) survey folder')
        # noinspection PyUnresolvedReferences
        button_input_folder.clicked.connect(self.click_add_input_folder)

        vbox.addSpacing(12)

        # buttons
        hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(hbox)
        hbox.addStretch()
        # xyz2fau
        button_xyz2fau = QtWidgets.QPushButton()
        hbox.addWidget(button_xyz2fau)
        button_xyz2fau.setFixedHeight(GuiSettings.single_line_height())
        # button_clear_data.setFixedWidth(GuiSettings.single_line_height())
        button_xyz2fau.setText("XYZ > FAU")
        button_xyz2fau.setToolTip('Convert xyz file into FAU format')
        # noinspection PyUnresolvedReferences
        button_xyz2fau.clicked.connect(self.click_xyz2fau)
        # clear data
        button_clear_data = QtWidgets.QPushButton()
        hbox.addWidget(button_clear_data)
        button_clear_data.setFixedHeight(GuiSettings.single_line_height())
        # button_clear_data.setFixedWidth(GuiSettings.single_line_height())
        button_clear_data.setText("Clear data")
        button_clear_data.setToolTip('Clear all data loaded')
        # noinspection PyUnresolvedReferences
        button_clear_data.clicked.connect(self.click_clear_data)
        hbox.addStretch()

        self.vbox.addStretch()
        self.vbox.addStretch()

        # data outputs
        self.savedData = QtWidgets.QGroupBox("Data outputs [drag-and-drop the desired output folder]")
        self.savedData.setStyleSheet("QGroupBox::title { color: rgb(155, 155, 155); }")
        self.savedData.setMaximumHeight(GuiSettings.single_line_height() * 8)
        self.vbox.addWidget(self.savedData)

        vbox = QtWidgets.QVBoxLayout()
        self.savedData.setLayout(vbox)

        # add folder
        hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(hbox)
        text_add_folder = QtWidgets.QLabel("Folder:")
        hbox.addWidget(text_add_folder)
        text_add_folder.setMinimumWidth(64)
        self.output_folder = QtWidgets.QListWidget()
        hbox.addWidget(self.output_folder)
        self.output_folder.setMinimumHeight(GuiSettings.single_line_height())
        self.output_folder.setMaximumHeight(GuiSettings.single_line_height() * 2)
        self.output_folder.clear()
        new_item = QtWidgets.QListWidgetItem()
        new_item.setIcon(QtGui.QIcon(os.path.join(self.parent_win.media, 'folder.png')))
        new_item.setText("%s" % os.path.abspath(self.prj._output_folder).replace("\\", "/"))
        new_item.setFont(GuiSettings.console_font())
        new_item.setForeground(GuiSettings.console_fg_color())
        self.output_folder.addItem(new_item)
        # Enable dropping onto the input grid list
        self.output_folder.setAcceptDrops(True)
        self.output_folder.installEventFilter(self)
        button_add_folder = QtWidgets.QPushButton()
        hbox.addWidget(button_add_folder)
        button_add_folder.setFixedHeight(GuiSettings.single_line_height())
        button_add_folder.setFixedWidth(GuiSettings.single_line_height())
        button_add_folder.setText(" .. ")
        button_add_folder.setToolTip('Add (or drag-and-drop) output folder')
        # noinspection PyUnresolvedReferences
        button_add_folder.clicked.connect(self.click_add_output_folder)

        # open folder
        hbox = QtWidgets.QHBoxLayout()
        vbox.addLayout(hbox)
        hbox.addStretch()

        button_default_output = QtWidgets.QPushButton()
        hbox.addWidget(button_default_output)
        button_default_output.setFixedHeight(GuiSettings.single_line_height())
        # button_open_output.setFixedWidth(GuiSettings.single_line_height())
        button_default_output.setText("Use default")
        button_default_output.setToolTip('Use the default output folder')
        # noinspection PyUnresolvedReferences
        button_default_output.clicked.connect(self.click_default_output)

        button_open_output = QtWidgets.QPushButton()
        hbox.addWidget(button_open_output)
        button_open_output.setFixedHeight(GuiSettings.single_line_height())
        # button_open_output.setFixedWidth(GuiSettings.single_line_height())
        button_open_output.setText("Open folder")
        button_open_output.setToolTip('Open the output folder')
        # noinspection PyUnresolvedReferences
        button_open_output.clicked.connect(self.click_open_output)

        hbox.addStretch()

    def eventFilter(self, obj, e):

        if e.type() == QtCore.QEvent.KeyPress:
            e.ignore()

            if e.modifiers() == QtCore.Qt.ControlModifier:
                pass

        # drag events
        elif (e.type() == QtCore.QEvent.DragEnter) or (e.type() == QtCore.QEvent.DragMove):

            if obj in (self.input_folder, self.output_folder):

                if e.mimeData().hasUrls:

                    for url in e.mimeData().urls():

                        dropped_path = str(url.toLocalFile())

                        dropped_path = os.path.abspath(dropped_path)

                        if os.path.isdir(dropped_path):
                            e.accept()
                            return True

            e.ignore()
            return True

        # drop events
        if e.type() == QtCore.QEvent.Drop:

            if obj is self.input_folder:

                if e.mimeData().hasUrls():

                    e.setDropAction(QtCore.Qt.CopyAction)
                    e.accept()
                    # Workaround for OSx dragging and dropping
                    for url in e.mimeData().urls():

                        dropped_path = str(url.toLocalFile())

                        dropped_path = os.path.abspath(dropped_path)

                        logger.debug("dropped file: %s" % dropped_path)
                        if os.path.isdir(dropped_path):
                            self._add_input_folder(selection=dropped_path)

                        else:
                            msg = 'Drag-and-drop is only possible with a single folder\n'
                            # noinspection PyCallByClass,PyArgumentList
                            QtWidgets.QMessageBox.critical(self, "Drag-and-drop Error", msg, QtWidgets.QMessageBox.Ok)

                    return True

            elif obj is self.output_folder:

                if e.mimeData().hasUrls():

                    e.setDropAction(QtCore.Qt.CopyAction)
                    e.accept()
                    # Workaround for OSx dragging and dropping
                    for url in e.mimeData().urls():

                        dropped_path = str(url.toLocalFile())

                        dropped_path = os.path.abspath(dropped_path)

                        logger.debug("dropped file: %s" % dropped_path)
                        if os.path.isdir(dropped_path):
                            self._add_output_folder(selection=dropped_path)

                        else:
                            msg = 'Drag-and-drop is only possible with a single folder\n'
                            # noinspection PyCallByClass,PyArgumentList
                            QtWidgets.QMessageBox.critical(self, "Drag-and-drop Error", msg, QtWidgets.QMessageBox.Ok)

                    return True

            e.ignore()
            return True

        return QtWidgets.QMainWindow.eventFilter(self, obj, e)

    def click_add_input_folder(self):
        """ Set the folder provided by the user"""
        logger.debug('set input folder ...')

        # ask the output folder
        # noinspection PyCallByClass
        selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set input folder",
                                                               QtCore.QSettings().value("survey_import_folder"), )
        if selection == "":
            logger.debug('setting input folder: aborted')
            return
        logger.debug("selected path: %s" % selection)

        self._add_input_folder(selection=selection)

    def _add_input_folder(self, selection):

        selection = os.path.abspath(selection).replace("\\", "/")

        # attempt to read the data
        try:
            self.prj.clear_submission_list()
            self.prj.submission_list.append(selection)

        except Exception as e:  # more general case that catches all the exceptions
            msg = '<b>Error setting the input folder to \"%s\".</b>' % selection
            msg += '<br><br><font color=\"red\">%s</font>' % e
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.critical(self, "Input Folder Error", msg, QtWidgets.QMessageBox.Ok)
            logger.debug('input folder NOT set: %s' % selection)
            return

        self.input_folder.clear()
        new_item = QtWidgets.QListWidgetItem()
        new_item.setIcon(QtGui.QIcon(os.path.join(self.parent_win.media, 'folder.png')))
        new_item.setText("%s" % self.prj.submission_list[0])
        new_item.setFont(GuiSettings.console_font())
        new_item.setForeground(GuiSettings.console_fg_color())
        self.input_folder.addItem(new_item)

        QtCore.QSettings().setValue("survey_import_folder", self.prj.submission_list[0])

        self.parent_win.folder_loaded()
        logger.debug("new input folder: %s" % self.prj.submission_list[0])

    def make_grids_context_menu(self, pos):
        logger.debug('context menu')

        # check if any selection
        sel = self.input_folder.selectedItems()

        if len(sel) == 0:
            # noinspection PyCallByClass, PyArgumentList
            QtWidgets.QMessageBox.information(self, "Survey folder", "You need to first add a survey folder!")
            return

        remove_act = QtGui.QAction("Remove", self, statusTip="Remove the selected grid files",
                                   triggered=self.remove_input_folder)

        menu = QtWidgets.QMenu(parent=self)
        # noinspection PyArgumentList
        menu.addAction(remove_act)
        # noinspection PyArgumentList
        menu.exec_(self.input_folder.mapToGlobal(pos))

    def remove_input_folder(self):
        logger.debug("user want to remove input folder")

        # remove all the selected files from the list
        selections = self.input_folder.selectedItems()
        for selection in selections:
            self.prj.remove_from_submission_list(selection.text())

        self.input_folder.clear()
        self.parent_win.folder_unloaded()

    def click_clear_data(self):
        """ Clear all the read data"""
        logger.debug('clear data')
        self.prj.clear_data()
        self.input_folder.clear()
        self.parent_win.folder_unloaded()

    def click_xyz2fau(self):
        """ Open the XYZ2FAU converter"""
        single_file = "Single file"
        folder = "Folder"
        item, ok = QtWidgets.QInputDialog.getItem(self, "FAU converter",
                                                  "Select input method",
                                                  [single_file, folder],
                                                  0, False)

        if ok:
            logger.debug('convert to FAU')
            x2f = Xyz2Fau(parent=self, folder_processing=item == folder)
            x2f.show()

    def click_add_output_folder(self):
        """ Set the folder provided by the user"""
        logger.debug('set output folder ...')

        # ask the output folder
        # noinspection PyCallByClass
        selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set output folder",
                                                               QtCore.QSettings().value("survey_export_folder"), )
        if selection == "":
            logger.debug('setting output folder: aborted')
            return
        logger.debug("selected path: %s" % selection)

        self._add_output_folder(selection=selection)

    def _add_output_folder(self, selection):

        selection = os.path.abspath(selection).replace("\\", "/")

        path_len = len(selection)
        logger.debug("folder path length: %d" % path_len)
        if path_len > 140:

            msg = 'The selected path is %d characters long. ' \
                  'This may trigger the filename truncation of generated outputs (max allowed path length: 260).\n\n' \
                  'Do you really want to use: %s?' % (path_len, selection)
            msg_box = QtWidgets.QMessageBox(self)
            msg_box.setWindowTitle("Output folder")
            msg_box.setText(msg)
            msg_box.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msg_box.setDefaultButton(QtWidgets.QMessageBox.No)
            reply = msg_box.exec_()

            if reply == QtWidgets.QMessageBox.No:
                return

        # attempt to read the data
        try:
            self.prj._output_folder = selection

        except Exception as e:  # more general case that catches all the exceptions
            msg = '<b>Error setting the output folder to \"%s\".</b>' % selection
            msg += '<br><br><font color=\"red\">%s</font>' % e
            # noinspection PyCallByClass,PyArgumentList
            QtWidgets.QMessageBox.critical(self, "Output Folder Error", msg, QtWidgets.QMessageBox.Ok)
            logger.debug('output folder NOT set: %s' % selection)
            return

        self.output_folder.clear()
        new_item = QtWidgets.QListWidgetItem()
        new_item.setIcon(QtGui.QIcon(os.path.join(self.parent_win.media, 'folder.png')))
        new_item.setText("%s" % self.prj._output_folder)
        new_item.setFont(GuiSettings.console_font())
        new_item.setForeground(GuiSettings.console_fg_color())
        self.output_folder.addItem(new_item)

        QtCore.QSettings().setValue("survey_export_folder", self.prj._output_folder)

        logger.debug("new output folder: %s" % self.prj._output_folder)

    def click_default_output(self):
        """ Set default output data folder """
        self._add_output_folder(selection=self.prj.default_output_folder())

    def click_open_output(self):
        """ Open output data folder """
        logger.debug('open output folder: %s' % self.prj._output_folder)
        self.prj.open_output_folder()
