import logging
import os
from typing import Optional

from PySide6 import QtWidgets, QtGui, QtCore

from oshydro.sussie.app.tabs.sussie.tabs.inputs import InputsTab
from oshydro.sussie.app.tabs.sussie.tabs.raw_data import RawDataTab
from oshydro.sussie.app.tabs.sussie.tabs.submission_checks import SubmissionTab
from oshydro.sussie.common.progress.qt_progress import QtProgress
from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class SussieTab(QtWidgets.QMainWindow):

    def __init__(self, main_win: Optional[QtWidgets.QMainWindow]):
        QtWidgets.QMainWindow.__init__(self)
        self.setFocusPolicy(QtCore.Qt.ClickFocus)

        self.main_win = main_win
        self.media = self.main_win.media

        # progress dialog
        self.progress = QtProgress(parent=self)
        self.prj = Project(progress=self.progress)

        self.setContentsMargins(0, 0, 0, 0)

        # add a frame
        self.frame = QtWidgets.QFrame()
        self.setCentralWidget(self.frame)

        # init default settings
        settings = QtCore.QSettings()
        # - import
        import_folder = settings.value("checks_import_folder")
        if (import_folder is None) or (not os.path.exists(import_folder)):
            settings.setValue("checks_import_folder", self.prj.output_folder)
        # - output folder
        export_folder = settings.value("checks_export_folder")
        if (export_folder is None) or (not os.path.exists(export_folder)):
            settings.setValue("checks_export_folder", self.prj.output_folder)
        else:  # folder exists
            self.prj.output_folder = export_folder

        # make tabs
        self.tabs = QtWidgets.QTabWidget()
        self.setCentralWidget(self.tabs)
        self.tabs.setContentsMargins(0, 0, 0, 0)
        self.tabs.setIconSize(QtCore.QSize(36, 36))
        self.tabs.setTabPosition(QtWidgets.QTabWidget.South)

        # - Inputs
        self.tab_inputs = InputsTab(parent_win=self, prj=self.prj)
        # noinspection PyArgumentList
        self.idx_inputs = self.tabs.insertTab(0, self.tab_inputs,
                                              QtGui.QIcon(os.path.join(self.media, 'inputs.png')), "")
        self.tabs.setTabToolTip(self.idx_inputs, "Survey Data Inputs")
        self.tabs.setTabEnabled(self.idx_inputs, True)

        # - Submission checks
        self.tab_submission = SubmissionTab(parent_win=self, prj=self.prj)
        # noinspection PyArgumentList
        self.idx_submission = self.tabs.insertTab(1, self.tab_submission,
                                                  QtGui.QIcon(os.path.join(self.media, 'submission.png')), "")
        self.tabs.setTabToolTip(self.idx_submission, "Submission checking")
        self.tabs.setTabEnabled(self.idx_submission, False)

        # - Raw data
        self.tab_raw_data = RawDataTab(parent_win=self, prj=self.prj)
        # noinspection PyArgumentList
        self.idx_raw_data = self.tabs.insertTab(3, self.tab_raw_data,
                                                QtGui.QIcon(os.path.join(self.media, 'raw_data.png')), "")
        self.tabs.setTabToolTip(self.idx_raw_data, "Exploring raw data")
        self.tabs.setTabEnabled(self.idx_raw_data, False)

        # noinspection PyUnresolvedReferences
        self.tabs.currentChanged.connect(self.change_tabs)

    def change_tabs(self, index):
        self.tabs.setCurrentIndex(index)
        self.tabs.currentWidget().setFocus()

    def change_info_url(self, url):
        self.main_win.change_info_url(url)

    def folder_loaded(self):
        self.tabs.setTabEnabled(self.idx_submission, True)
        self.tabs.setTabEnabled(self.idx_raw_data, True)
        self.tab_submission.folder_changed()
        self.tab_raw_data.folder_changed()

    def folder_unloaded(self):
        self.tabs.setTabEnabled(self.idx_submission, False)
        self.tabs.setTabEnabled(self.idx_raw_data, False)
        self.tab_submission.folder_changed()
        self.tab_raw_data.folder_changed()
