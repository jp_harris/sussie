from PySide6 import QtWidgets

import logging

from oshydro.sussie.app.app_style import AppStyle
from oshydro.sussie.common.helper import Helper

logger = logging.getLogger(__name__)


class GeneralInfoTab(QtWidgets.QWidget):

    def __init__(self, parent: QtWidgets.QWidget=None):
        super().__init__(parent)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.text = QtWidgets.QTextBrowser()
        self.text.setReadOnly(True)
        self.text.setMinimumWidth(200)
        self.text.setOpenLinks(True)
        self.text.setOpenExternalLinks(True)
        self.text.document().setDefaultStyleSheet(AppStyle.html_css())
        self.text.setHtml(Helper.package_info(qt_html=True))
        self.layout.addWidget(self.text)
