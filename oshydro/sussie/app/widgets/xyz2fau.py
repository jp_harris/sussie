import logging
import os
from typing import List, Optional, Tuple

from PySide6 import QtWidgets, QtGui, QtCore

from oshydro.sussie.common.helper import Helper
from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class Xyz2Fau(QtWidgets.QMainWindow):
    media = os.path.join(os.path.dirname(__file__), 'media')

    def __init__(self, parent: QtWidgets = None, title: Optional[str] = "FAU Converter",
                 folder_processing: Optional[bool] = False) -> None:
        QtWidgets.QMainWindow.__init__(self, parent)

        if isinstance(title, str):
            self.setWindowTitle(title)

        self._folder_processing = folder_processing

        if parent is None:
            icon_info = QtCore.QFileInfo(os.path.join(self.media, 'favicon.png'))
            self.setWindowIcon(QtGui.QIcon(icon_info.absoluteFilePath()))
            if Helper.is_windows():

                try:
                    # This is needed to display the app icon on the taskbar on Windows 7
                    import ctypes
                    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(title)

                except AttributeError as e:
                    logger.debug("Unable to change app icon: %s" % e)

        # noinspection PyArgumentList
        self.setMinimumSize(400, 350)
        # noinspection PyArgumentList
        self.resize(600, 400)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 100
        wdg_width = 80

        # input filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_files = QtWidgets.QLabel("Input path:")

        hbox.addWidget(text_files)
        text_files.setFixedWidth(lbl_width)
        self.input_path = QtWidgets.QListWidget()
        self.input_path.setFixedHeight(24)
        hbox.addWidget(self.input_path)
        self.input_path.setAlternatingRowColors(True)
        input_button = QtWidgets.QPushButton("..")
        input_button.setFixedWidth(30)
        hbox.addWidget(input_button)
        input_button.clicked.connect(self.on_set_input_path)

        # output filename
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_output = QtWidgets.QLabel("Output path:")
        hbox.addWidget(text_output)
        text_output.setFixedWidth(lbl_width)
        self.output_path = QtWidgets.QListWidget()
        self.output_path.setFixedHeight(24)
        hbox.addWidget(self.output_path)
        self.output_path.setAlternatingRowColors(True)
        output_button = QtWidgets.QPushButton("..")
        output_button.setFixedWidth(30)
        hbox.addWidget(output_button)
        output_button.clicked.connect(self.on_set_output_path)

        if self._folder_processing:
            # Scan folders recursive
            hbox = QtWidgets.QHBoxLayout()
            self.vbox.addLayout(hbox)
            text_skip = QtWidgets.QLabel("Scan subfolders:")
            hbox.addWidget(text_skip)
            text_skip.setFixedWidth(lbl_width)
            self.set_recursive = QtWidgets.QCheckBox()
            self.set_recursive.setChecked(False)
            hbox.addWidget(self.set_recursive)
            hbox.addStretch()

            # Overwrite existing files
            hbox = QtWidgets.QHBoxLayout()
            self.vbox.addLayout(hbox)
            text_skip = QtWidgets.QLabel("Overwrite files:")
            hbox.addWidget(text_skip)
            text_skip.setFixedWidth(lbl_width)
            self.set_overwrite = QtWidgets.QCheckBox()
            self.set_overwrite.setChecked(False)
            hbox.addWidget(self.set_overwrite)
            hbox.addStretch()
            print("Overwrite?", self.set_recursive.isChecked())

        self.vbox.addSpacing(12)

        # skip row
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_skip = QtWidgets.QLabel("Skip rows:")
        hbox.addWidget(text_skip)
        text_skip.setFixedWidth(lbl_width)
        self.set_skip = QtWidgets.QSpinBox()
        self.set_skip.setFixedWidth(wdg_width)
        self.set_skip.setMaximum(9999)
        self.set_skip.setValue(1)
        hbox.addWidget(self.set_skip)
        hbox.addStretch()

        # separator
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_separator = QtWidgets.QLabel("Column separator:")
        hbox.addWidget(text_separator)
        text_separator.setFixedWidth(lbl_width)
        self.set_separator = QtWidgets.QComboBox()
        self.set_separator.setFixedWidth(wdg_width)
        self.set_separator.addItems([',', ';', 'TAB', 'SPACE'])
        hbox.addWidget(self.set_separator)
        hbox.addStretch()

        # set xyz columns
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_columns = QtWidgets.QLabel("Column Indices:")
        hbox.addWidget(text_columns)
        text_columns.setFixedWidth(lbl_width)
        # x
        text_easting = QtWidgets.QLabel("x")
        hbox.addWidget(text_easting)
        text_easting.setFixedWidth(5)
        self.set_easting = QtWidgets.QSpinBox()
        self.set_easting.setFixedWidth(70)
        self.set_easting.setMaximum(9999)
        self.set_easting.setValue(0)
        hbox.addWidget(self.set_easting)
        hbox.addSpacing(20)
        # y
        text_northing = QtWidgets.QLabel("y")
        hbox.addWidget(text_northing)
        text_northing.setFixedWidth(5)
        self.set_northing = QtWidgets.QSpinBox()
        self.set_northing.setFixedWidth(70)
        self.set_northing.setMaximum(9999)
        self.set_northing.setValue(1)
        hbox.addWidget(self.set_northing)
        hbox.addSpacing(20)
        # z
        text_depth = QtWidgets.QLabel("z")
        hbox.addWidget(text_depth)
        text_depth.setFixedWidth(5)
        self.set_depth = QtWidgets.QSpinBox()
        self.set_depth.setFixedWidth(70)
        self.set_depth.setMaximum(9999)
        self.set_depth.setValue(2)
        hbox.addWidget(self.set_depth)
        hbox.addStretch()

        # z type
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_z_type = QtWidgets.QLabel("Z type:")
        hbox.addWidget(text_z_type)
        text_z_type.setFixedWidth(lbl_width)
        self.set_z_type = QtWidgets.QComboBox()
        self.set_z_type.setFixedWidth(wdg_width)
        self.set_z_type.addItems(['depth', 'elevation'])
        hbox.addWidget(self.set_z_type)
        hbox.addStretch()

        # depth shift
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_shift = QtWidgets.QLabel("Apply shift:")
        hbox.addWidget(text_shift)
        text_shift.setFixedWidth(lbl_width)
        self.set_shift = QtWidgets.QDoubleSpinBox()
        self.set_shift.setFixedWidth(wdg_width)
        self.set_shift.setMinimum(-9999.0)
        self.set_shift.setMaximum(9999.0)
        self.set_shift.setDecimals(3)
        self.set_shift.setValue(0.0)
        hbox.addWidget(self.set_shift)
        hbox.addStretch()

        # FAU endianess
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_endianess = QtWidgets.QLabel("FAU endianess:")
        hbox.addWidget(text_endianess)
        text_endianess.setFixedWidth(lbl_width)
        self.set_endianess = QtWidgets.QComboBox()
        self.set_endianess.setFixedWidth(wdg_width)
        self.set_endianess.addItems(['little', 'big'])
        hbox.addWidget(self.set_endianess)
        hbox.addStretch()

        self.vbox.addSpacing(12)

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        export_button = QtWidgets.QPushButton('Convert')
        hbox.addWidget(export_button)
        # noinspection PyUnresolvedReferences
        export_button.clicked.connect(self.on_convert)
        hbox.addStretch()

    def on_set_input_path(self) -> None:
        logger.debug('setting input file/folder ...')

        # noinspection PyCallByClass
        if self._folder_processing:
            selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set input folder",
                                                                   QtCore.QSettings().value("xyz2fau_input_folder"))
        else:
            selection, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Set input text file",
                                                                 QtCore.QSettings().value("xyz2fau_input_folder"),
                                                                 "Text Files (*.asc *.txt *.xyz *.pts)")
        if selection == "":
            logger.debug('setting input: aborted')
            return
        self.input_path.clear()
        self.input_path.addItem(selection)
        QtCore.QSettings().setValue("xyz2fau_input_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)

        self.output_path.clear()
        if self._folder_processing:
            self.output_path.addItem("%s" % selection)
        else:
            self.output_path.addItem("%s.fau" % os.path.splitext(selection)[0])

    def on_set_output_path(self) -> None:
        logger.debug('setting output file ...')

        # noinspection PyCallByClass
        if self._folder_processing:
            selection = QtWidgets.QFileDialog.getExistingDirectory(self, "Set output text folder",
                                                                   QtCore.QSettings().value("xyz2fau_input_folder"))
        else:
            selection, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Set output text file",
                                                                 QtCore.QSettings().value("xyz2fau_output_folder"),
                                                                 "FAU Files (*.fau)")
        if selection == "":
            logger.debug('setting output folder: aborted')
            return
        self.output_path.clear()
        self.output_path.addItem(selection)
        QtCore.QSettings().setValue("xyz2fau_output_folder", os.path.dirname(selection))
        logger.debug("selected path: %s" % selection)

    def on_convert(self) -> None:
        logger.debug('converting xyz file(s) ...')

        if self.input_path.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Input issue", "First set the input path",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        if self.output_path.count() == 0:
            QtWidgets.QMessageBox.warning(self, "Output issue", "First set the output path",
                                          QtWidgets.QMessageBox.StandardButton.Ok)
            return

        if self._folder_processing:
            self._on_convert_folder()
        else:
            self._on_convert_file()

    def _on_convert_file(self) -> None:
        to_convert = [(self.input_path.item(0).text(), self.output_path.item(0).text())]
        self._convert(to_convert)
        QtWidgets.QMessageBox.information(self, "Output created", "File converted!",
                                          QtWidgets.QMessageBox.StandardButton.Ok)

    def _on_convert_folder(self) -> None:
        to_convert = []
        input_folder = self.input_path.item(0).text()
        output_folder = self.output_path.item(0).text()
        files_skipped = 0
        for base, folders, files in os.walk(os.path.abspath(input_folder)):
            for file in files:
                if os.path.splitext(file)[1].lower() not in (".xyz", ".asc", ".txt", ".pts"):
                    continue

                input_path = os.path.join(base, file)
                output_path = os.path.join('%s.fau' % os.path.splitext(input_path)[0])
                logger.debug("Storing converted file as {}".format(output_path))

                if output_folder is not None:
                    output_path = output_path.replace(os.path.abspath(input_folder),
                                                      os.path.abspath(output_folder))
                    os.makedirs(os.path.dirname(output_path), exist_ok=True)

                if not self.set_overwrite.isChecked() and os.path.exists(output_path):
                    logger.warning("Fau file already exists. Skipping: {}".format(output_path))
                    files_skipped += 1
                    continue
                to_convert.append((input_path, output_path))
            if not self.set_recursive.isChecked():
                break
        self._convert(to_convert)
        QtWidgets.QMessageBox.information(self, "Output created",
                                          "{} files converted!\n{} files skipped!".format(len(to_convert),
                                                                                          files_skipped),
                                          QtWidgets.QMessageBox.StandardButton.Ok)

    def _convert(self, to_convert: List[Tuple[str, str]]) -> None:
        depth_shift = self.set_shift.value()
        if depth_shift == 0.0:
            depth_shift = None

        split_value = self.set_separator.currentText()
        if split_value == 'TAB':
            split_value = '\t'
        elif split_value == 'SPACE':
            split_value = None

        prj = Project()
        for input_file, output_file in to_convert:
            ret = prj.convert_from_xyz(input_path=input_file,
                                       output_path=output_file,
                                       skip_rows=self.set_skip.value(),
                                       little_endian=(self.set_endianess.currentText() == "little"),
                                       depth_shift=depth_shift,
                                       easting_idx=self.set_easting.value(),
                                       northing_idx=self.set_northing.value(),
                                       depth_idx=self.set_depth.value(),
                                       split_value=split_value,
                                       z_is_elevation=(self.set_z_type.currentText() == "elevation"))
            logger.debug('generated: %s' % ret)
