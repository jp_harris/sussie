import logging
import os
from typing import List, Optional

from PySide6 import QtWidgets

from oshydro.sussie import __version__
from oshydro.sussie.survey.project import Project

logger = logging.getLogger(__name__)


class PositionChecker(QtWidgets.QMainWindow):

    def __init__(self, files: List[str], prj: Project, parent: QtWidgets = None,
                 title: Optional[str] = "Position Checker"):
        QtWidgets.QMainWindow.__init__(self, parent)

        if isinstance(title, str):
            self.setWindowTitle(title)

        for f in files:
            if not os.path.exists(f) or not os.path.isfile(f):
                raise RuntimeError('Unable to locate %s' % f)

        self._files = files
        self._prj = prj

        # noinspection PyArgumentList
        self.setMinimumSize(300, 100)
        # noinspection PyArgumentList
        self.resize(300, 150)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.vbox = QtWidgets.QVBoxLayout()
        frame.setLayout(self.vbox)

        lbl_width = 230

        # check for positional outliers
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_outliers = QtWidgets.QLabel("Check for positional outliers:")
        hbox.addWidget(text_outliers)
        text_outliers.setMinimumWidth(lbl_width)
        self.check_outliers = QtWidgets.QCheckBox()
        self.check_outliers.setChecked(True)
        hbox.addWidget(self.check_outliers)
        hbox.addStretch()

        # check for duplicates
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_duplicates = QtWidgets.QLabel("Check for duplicate points:")
        hbox.addWidget(text_duplicates)
        text_duplicates.setMinimumWidth(lbl_width)
        self.check_duplicates = QtWidgets.QCheckBox()
        self.check_duplicates.setChecked(True)
        hbox.addWidget(self.check_duplicates)
        hbox.addStretch()

        # save all outlier/duplicate points to json
        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        text_duplicates = QtWidgets.QLabel("Write full JSON including all issue-points:")
        hbox.addWidget(text_duplicates)
        text_duplicates.setMinimumWidth(lbl_width)
        self.full_json = QtWidgets.QCheckBox()
        self.full_json.setChecked(False)
        hbox.addWidget(self.full_json)
        hbox.addStretch()

        self.vbox.addStretch()

        hbox = QtWidgets.QHBoxLayout()
        self.vbox.addLayout(hbox)
        hbox.addStretch()
        check_button = QtWidgets.QPushButton('Check positions')
        hbox.addWidget(check_button)
        # noinspection PyUnresolvedReferences
        check_button.clicked.connect(self.check_positions)
        hbox.addStretch()

    def check_positions(self) -> None:
        logger.debug('checking file positions for outliers')
        check_outliers = self.check_outliers.isChecked()
        check_duplicates = self.check_duplicates.isChecked()
        full_json = self.full_json.isChecked()

        if not any([check_outliers, check_duplicates]):
            QtWidgets.QMessageBox.information(self, "Check positions v.%s" % __version__,
                                              "At least one check must be selected",
                                              QtWidgets.QMessageBox.Ok)
            return

        if check_outliers:
            logger.debug("Checking for positional outliers: %s" % check_outliers)
        if check_duplicates:
            logger.debug("Checking for positional duplicates %s" % check_duplicates)

        pos_analyzer = self._prj.check_positions(paths=self._files, root_path=self._prj.submission_list[0],
                                                 check_outliers=check_outliers,
                                                 check_duplicates=check_duplicates,
                                                 full_json=full_json
                                                 )
        if self._prj.progress.canceled:
            return

        logger.debug('results: %s' % pos_analyzer.report)

        # noinspection PyCallByClass,PyArgumentList
        msg = ""
        if check_outliers:
            msg = ("Identified %d flagged / %d unflagged positional outliers in %d files." %
                   (pos_analyzer.outliers_flagged, pos_analyzer.outliers_unflagged, pos_analyzer.outliers_files))
        if check_duplicates:
            msg += ("Identified %d flagged / %d unflagged duplicates in %d files." %
                    (pos_analyzer.duplicates_flagged, pos_analyzer.duplicates_unflagged,
                     pos_analyzer.duplicates_files))
        if pos_analyzer.error_files > 0:
            msg += 'Check the json file (%s) for more info.' % (os.path.basename(pos_analyzer.report_path),)
        self._prj.open_output_folder()
        QtWidgets.QMessageBox.information(self, "Check positions v.%s" % __version__, msg, QtWidgets.QMessageBox.Ok)
        self.close()

    def on_change_outliers(self) -> None:
        if self.check_outliers.isChecked():
            logger.debug('Check outliers: True')

    def on_change_duplicates(self) -> None:
        if self.check_duplicates.isChecked():
            logger.debug('selected output: .shp')
