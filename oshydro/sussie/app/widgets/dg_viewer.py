import logging
from typing import Optional

from PySide6 import QtWidgets

logger = logging.getLogger(__name__)


class DatagramViewer(QtWidgets.QMainWindow):

    def __init__(self, data_dict: dict, var_dict: dict, parent: QtWidgets = None,
                 title: Optional[str] = None):
        QtWidgets.QMainWindow.__init__(self, parent)
        if isinstance(title, str):
            self.setWindowTitle(title)

        self._d = data_dict
        d_size = len(self._d['fields'])
        # logger.debug('data: %d -> %s' % (d_size, self._d))

        self._v = var_dict
        v_size = len(self._v['fields'])
        # logger.debug('variant: %d -> %s' % (v_size, self._v['fields']))

        if d_size != v_size:
            raise RuntimeError('data fields does not match the variant fields: %d != %d' % (d_size, v_size))

        col_items = 20
        nr_cols = d_size // col_items + 1
        # logger.debug('nr of cols: %s' % nr_cols)

        frame = QtWidgets.QFrame()
        self.setCentralWidget(frame)

        self.hbox = QtWidgets.QHBoxLayout()
        frame.setLayout(self.hbox)

        d_list = list(self._d['fields'].items())
        v_list = list(self._v['fields'].items())

        vboxes = list()
        for idx in range(nr_cols):

            vboxes.append(QtWidgets.QVBoxLayout())
            self.hbox.addLayout(vboxes[idx])

            if idx != (nr_cols - 1):
                self.hbox.addSpacing(10)

        for idx in range(len(d_list)):

            col = idx // col_items
            # logger.debug('col: %s' % col)

            hbox = QtWidgets.QHBoxLayout()
            vboxes[col].addLayout(hbox)
            label = QtWidgets.QLabel(d_list[idx][0].replace('_', ' '))
            hbox.addWidget(label)

            # logger.info('%s' % v_list[idx][1])
            if 's' in v_list[idx][1]['type']:
                raw = d_list[idx][1].decode(errors='replace')
                raw_val = '%s' % raw.rstrip("\x00")
                # skipping unprintable characters
                val = "".join([c if 0x21 <= ord(c) <= 0x7e else "" for c in raw])
                if raw_val != val:
                    logger.warning('%s: unprintable values -> %s' % (d_list[idx][0], raw_val))
            else:
                val = '%s' % d_list[idx][1]
            value = QtWidgets.QLineEdit(val)
            value.setDisabled(True)
            value.setToolTip(v_list[idx][1]['comment'])
            hbox.addWidget(value)

        for idx in range(nr_cols):
            vboxes[idx].addStretch()
