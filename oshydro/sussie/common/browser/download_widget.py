import sys

from PySide6 import QtCore, QtGui, QtWidgets, QtWebEngineCore


class DownloadWidget(QtWidgets.QProgressBar):
    finished = QtCore.Signal()
    remove_requested = QtCore.Signal()

    def __init__(self, download_item: QtWebEngineCore.QWebEngineDownloadRequest) -> None:
        super(DownloadWidget, self).__init__()
        self._download_item = download_item
        # noinspection PyUnresolvedReferences
        download_item.isFinishedChanged.connect(self._finished)
        # noinspection PyUnresolvedReferences
        download_item.receivedBytesChanged.connect(self._download_progress)
        # noinspection PyUnresolvedReferences
        download_item.stateChanged.connect(self._update_tool_tip())
        path = download_item.downloadDirectory()

        self.setMaximumWidth(300)
        # Shorten the file name
        description = QtCore.QFileInfo(path).fileName()
        description_length = len(description)
        if description_length > 30:
            description = '{}...{}'.format(description[0:10], description[description_length - 10:])

        self.setFormat('{} %p%'.format(description))
        self.setOrientation(QtCore.Qt.Horizontal)
        self.setMinimum(0)
        self.setValue(0)
        self.setMaximum(100)
        self._update_tool_tip()

        # Force progress bar text to be shown on macoS by using 'fusion' style
        if sys.platform == 'darwin':
            # noinspection PyCallByClass,PyTypeChecker,PyArgumentList
            self.setStyle(QtWidgets.QStyleFactory.create('fusion'))

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        # noinspection PyUnresolvedReferences
        self.customContextMenuRequested.connect(self.context_menu_event)

    @classmethod
    def open_file(cls, file: str) -> None:
        # noinspection PyTypeChecker,PyCallByClass,PyArgumentList
        QtGui.QDesktopServices.openUrl(QtCore.QUrl.fromLocalFile(file))

    @classmethod
    def open_download_directory(cls) -> None:
        # noinspection PyArgumentList
        path = QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.DownloadLocation)
        cls.open_file(path)

    def _finished(self) -> None:
        self._update_tool_tip()
        # noinspection PyUnresolvedReferences
        self.finished.emit()

    def _download_progress(self) -> None:
        self.setValue(int(100 * self._download_item.receivedBytes() / self._download_item.totalBytes()))

    def _update_tool_tip(self) -> None:
        path = self._download_item.downloadDirectory()
        # noinspection PyArgumentList
        tool_tip = "{}\n{}".format(self._download_item.url().toString(),
                                   QtCore.QDir.toNativeSeparators(path))
        total_bytes = self._download_item.totalBytes()
        if total_bytes > 0:
            tool_tip += "\n{}K".format(total_bytes / 1024)
        state = self._download_item.state()
        if state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadRequested:
            tool_tip += "\n(requested)"
        elif state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadInProgress:
            tool_tip += "\n(downloading)"
        elif state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadCompleted:
            tool_tip += "\n(completed)"
        elif state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadCancelled:
            tool_tip += "\n(cancelled)"
        else:
            tool_tip += "\n(interrupted)"

        self.setToolTip(tool_tip)

    def _launch(self) -> None:
        self.open_file(self._download_item.downloadDirectory())

    def mouseDoubleClickEvent(self, event: QtGui.QMouseEvent) -> None:

        if self._download_item.state() == QtWebEngineCore.QWebEngineDownloadRequest.DownloadCompleted:
            self._launch()

        event.ignore()

    def context_menu_event(self, event: QtGui.QMouseEvent) -> None:
        state = self._download_item.state()
        context_menu = QtWidgets.QMenu()
        # noinspection PyArgumentList
        launch_action = context_menu.addAction("Launch")
        launch_action.setEnabled(state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadCompleted)
        # noinspection PyArgumentList
        show_in_folder_action = context_menu.addAction("Show in Folder")
        show_in_folder_action.setEnabled(state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadCompleted)
        # noinspection PyArgumentList
        cancel_action = context_menu.addAction("Cancel")
        cancel_action.setEnabled(state == QtWebEngineCore.QWebEngineDownloadRequest.DownloadInProgress)
        # noinspection PyArgumentList
        remove_action = context_menu.addAction("Remove")
        remove_action.setEnabled(state != QtWebEngineCore.QWebEngineDownloadRequest.DownloadInProgress)

        # noinspection PyArgumentList
        chosen_action = context_menu.exec_(self.mapToGlobal(event))
        if chosen_action == launch_action:
            self._launch()
        elif chosen_action == show_in_folder_action:
            self.open_file(QtCore.QFileInfo(self._download_item.downloadDirectory()).absolutePath())
        elif chosen_action == cancel_action:
            self._download_item.cancel()
            self.remove_requested.emit()
        elif chosen_action == remove_action:
            # noinspection PyUnresolvedReferences
            self.remove_requested.emit()
