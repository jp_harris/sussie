from PySide6 import QtWidgets, QtCore


class CustomQSpinBox(QtWidgets.QSpinBox):
    """Subclass of QtWidgets.QSpinBox which catches custom keyPressEvents"""

    def __init__(self):
        super().__init__()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Home:
            self.lineEdit().setText(str(self.minimum()))
        elif event.key() == QtCore.Qt.Key_End:
            self.lineEdit().setText(str(self.maximum()))
        else:
            super().keyPressEvent(event)
