import logging
import os

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example takes a .xyz file and create a header-less FAU file.

# Set a path to a xyz file and prepare the output FAU path

testing = Testing()
input_paths = testing.input_test_files(ext='.xyz', subfolder='xyz')
logger.debug("nr. of files: %s" % len(input_paths))
input_path = input_paths[0]
logger.debug('input: %s' % input_path)
output_path = os.path.join(testing.output_data_folder(), '%s.fau' % os.path.splitext(os.path.basename(input_path))[0])
logger.debug('output: %s' % output_path)

# Actually creating the FAU file

prj = Project()
ret = prj.convert_from_xyz(input_path=input_path, output_path=output_path, skip_rows=1,
                           little_endian=True, depth_shift=None, z_is_elevation=True)
logger.debug('generated: %s' % ret)
