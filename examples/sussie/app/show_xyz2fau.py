import logging

from PySide6 import QtWidgets

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.app.widgets.xyz2fau import Xyz2Fau

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example visualizes the XYZ2FAU converter to generate a FAU from a xyz file or folder

folder_processing = False

app = QtWidgets.QApplication([])

x2f = Xyz2Fau(folder_processing=folder_processing)
x2f.show()

exit(app.exec())
