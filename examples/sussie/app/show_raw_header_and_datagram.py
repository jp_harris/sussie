import logging
import os

from PySide6 import QtWidgets

from oshydro.sussie.common.testing import Testing
from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.survey.raw.raw import Raw
from oshydro.sussie.app.widgets.dg_viewer import DatagramViewer

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example visualizes the header and the first datagram of a .fau file with header

# Set the path to a .fau file with header

testing = Testing()
input_path = os.path.join(testing.input_data_folder(), 'fau', 'valid_le_with_header.fau')
if not os.path.exists(input_path):
    raise RuntimeError('Unable to locate %s' % input_path)
logger.debug("Input path: %s" % input_path)

# Open the .fau file

raw = Raw(path=input_path)
raw.check_variant()
raw.read_header()
raw.read_body()

# Create a Qt application, then use the DatagramViewer to visualize header and first datagram

app = QtWidgets.QApplication([])

rv = DatagramViewer(data_dict=raw.header.d, var_dict=raw.header.v['header'], title='test')
rv.show()

dg = raw.body.get_datagram(0)
rv2 = DatagramViewer(data_dict=dg, var_dict=raw.body.v['body'])
rv2.show()

exit(app.exec())
