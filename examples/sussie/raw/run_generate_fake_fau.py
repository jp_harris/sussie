import logging
import os

from oshydro.sussie.common.testing import Testing
from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.survey.raw.raw import Raw

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

nr_of_soundings = 10240

testing = Testing()
output_path = os.path.join(testing.output_data_folder(), '%d.fau' % nr_of_soundings)
if os.path.exists(output_path):
    os.remove(output_path)
logger.debug("Output FAU: %s" % output_path)

raw = Raw(path=output_path)
raw.force_little_endianess()
raw.generate_fake_fau(nr_of_soundings=nr_of_soundings, with_spikes=True)
