import logging

from oshydro.sussie.common.testing import Testing
from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.survey.raw.raw import Raw

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example shows how to use the Raw class to read a .fau file, by datagram or by ping-beam

testing = Testing()
input_paths = testing.input_test_files(ext='.fau', subfolder='fau')
logger.debug("nr. of files: %s" % len(input_paths))

input_path = input_paths[0]
logger.debug('input: %s' % input_path)

explore_datagram = True
explore_sounding = False  # only for FAU with header

raw = Raw(path=input_path)
raw.check_variant()
raw.read_header()
raw.read_body()
nr_of_dg = raw.body.nr_of_datagrams
logger.debug("nr of datagrams: %s" % nr_of_dg)

if explore_datagram:
    idx = 0
    dg = raw.body.get_datagram(idx=idx)
    logger.debug("datagram #%d -> %s" % (idx, dg))
    logger.debug("datetime -> %s"
                 % (Raw.datetime_from_unix_time_and_centisec(dg['fields']['sec'], dg['fields']['centisec'])))

if explore_sounding:
    ping = 0
    beam = 0
    dg = raw.body.get_sounding(ping=ping, beam=beam)
    logger.debug("sounding @%d:%d -> %s" % (ping, beam, dg))
    logger.debug("datetime -> %s"
                 % (Raw.datetime_from_unix_time_and_centisec(dg['fields']['sec'], dg['fields']['centisec'])))

raw.body.close_source()
