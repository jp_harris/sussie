import logging

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.survey.raw.formats.formats import Formats

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example helps to view and create the .json containing file format description

create_new_format = False
if create_new_format:
    fmt_name = 'new'
    fmt_exts = ['.new', ]
    fmt = Formats.new(name=fmt_name, exts=fmt_exts)
    Formats.set_both_endianess(fmt['variants'][0], first_types='8s', first_is_little='fau__uaf')
    json_path = Formats.dump_to_json(fmt=fmt)
    logger.debug('dumped new format: %s' % json_path)

# list formats
files = Formats.list()
logger.debug('nr of formats: %s' % len(files))
for f in files:
    logger.debug(' - %s' % f)

    # read a format
    loaded_fmt = Formats.load_from_json(json_path=f)
    logger.debug('   loaded: %s' % (loaded_fmt, ))

# list by extension
ed = Formats.ext_dict()
logger.debug('ext dict: %s' % (ed, ))
